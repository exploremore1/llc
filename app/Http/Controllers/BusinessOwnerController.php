<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class BusinessOwnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('type', 'business_owner')->orderBy('lname', 'asc')->get();
        return view('business_owners.index',compact('users'));
    }
    
    /**
     * Method: Add/Edit User data and password based on Type param
     *
     * @param  int  $id
     * @param  string  $type
     * @return array JsonResponse
     */
    public function edit(Request $request, $id = "", $type = "")
    {
        if ($request->isMethod('get')) 
        {
            $users = User::find($id);            
            $view = (!empty($type) && ($type == "password"))?"business_owners.editpassword" :"business_owners.edit";
            
            return response()->json([
                'result' => true,
                'data' => view($view)->with("id",$id)->with('users',$users)->render()
                ]);
        }
        
        $data = $request->all();
        $data['type'] = 'business_owner';
        $data['task'] = 'save_business_owner';

        if(empty($type) ) {
            if(!empty($id))
            {
                $validator = Validator::make($data, [
                    'email' => ['required',Rule::unique('users')->ignore($id)],
                    ])->validate();   
            }else{               
                $validator = Validator::make($data, [
                    'email' => 'required|unique:users',
                    ])->validate();   
            }
        }

        unset($data["_token"]);
        if(!empty($id)){
            $data["id"] = $id;
            
            //Update Password          
            if(!empty($type) && ($type == "password"))
            {
                if($data["password_confirmation"] != $data["password"])
                {
                    $error_msg = "Password and confirm password should be same";
                }
                
                if(!empty($error_msg))
                {
                    return response()->json([ 'result' => false,
                        'data' => $error_msg
                    ]);
                }

                unset($data["password_confirmation"]);
                User::saveUser($data);
                
                return response()->json([ 'result' => true,
                                      'view' => view("business_owners.edit")->render(),
                                      'action' => 'change_password',
                                      'user_type' => auth()->user()->type,
                                      'data' => "Password has been updated successfully."
                ]);                
            }
        }
        
        User::saveUser($data,$type);
        $users = User::find(auth()->user()->id);
            
        $view = (!empty($type) && ($type == "password"))?"business_owners.editpassword" :((!empty($type) && ($type == "profile"))?"business_owners.profileview" :"business_owners.edit");

        return response()->json([  'result' => true,
                                    'action' => 'edit_profile',
                                    'view' => view($view)->with('users',$users)->render(),
                                    'data' => "Your profile has been updated successfully."
        ]);
        
        
    }

    /**
     * Method: Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array JsonResponse
     */
    public function delete($id = null)
    {
        $ticket = User::find($id);
        $ticket->delete();
        return response()->json([ 
            'result' => true,
            'data' => "Record has been deleted!"
        ]);
    }
    
    /**
     * Method: Find the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array JsonResponse
     */
    public function find(Request $request)
    {
        $data = $request->all();
        
        $users = User::findby($data)->where('type', 'business_owner');
        return response()->json([
            'result' => true,
            'data' => view('business_owners.list')->with('users',$users)->render()
        ]);
    }

     /**
     * Method: Find the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array JsonResponse
     */
    public function view($id = "")
    {
        if(!empty($id))
        {
            $businessOwner = User::find($id);
            return view('business_owners.reportViewDetails',compact('id','businessOwner'));
        }
    }
}
