<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Song;
use App\Models\Reward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


use Illuminate\Support\Facades\Storage;
use App\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Mail\SendingEmail;
use Illuminate\Support\Facades\Mail;

class RewardController extends Controller
{   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $business_owners='';
        if(auth()->user()->type == 'admin'){
            $business_owners = User::where('type', 'business_owner')->where('active', '1')->orderBy('lname', 'asc')->get();
            $customers = User::where('type', 'customer')->where('active', '1')->orderBy('lname', 'asc')->get(); 
            $result = Reward::fetchall();
        }else if(auth()->user()->type == 'business_owner'){
            $business_owners = User::find(auth()->user()->id);
            $users = User::with(['assignedBusinessOwners:users.id,users.fname,users.lname'])->find(auth()->user()->id);
            $ids = array();         
            foreach($users->assignedBusinessOwners as $own) {
                $ids[] = $own['id'];
            }
            $customers = User::where('type', 'customer')->where('active', '1')->whereIn('id', $ids)->orderBy('lname', 'asc')->get(); 
            $data['business_owner_id'] = auth()->user()->id;
            $result = Reward::fetchall($data);
        }else if(auth()->user()->type == 'customer'){
            $customers = User::where('type', 'customer')->where('active', '1')->where('id', auth()->user()->id)->orderBy('lname', 'asc')->get(); 
            $data['customer_id'] = auth()->user()->id;
            $result = Reward::fetchall($data);
        }
        $songs = Song::fetchall(array('status'=>'1'))['data'];
        
       //print_r($result);die('here');
        if($result['result']){
            $records = $result['data'];
            return view('rewards.index',compact('records', 'business_owners', 'songs','customers'));
        }else{
            return $result;
        }
    }
    
    /**
     * Method: Add/Edit 
     *
     * @param  int  $id
     * @return array JsonResponse
     */
    public function edit(Request $request, $id = "", $requestType="")
    {

        if($requestType == 'get_assigned_customers'){            
            $users = User::with(['assignedBusinessOwners:users.id,users.fname,users.lname'])->find($id);
            $ids = array();         
            foreach($users->assignedBusinessOwners as $own) {
                $ids[] = $own['id'];
            }
            $customers = User::where('type', 'customer')->where('active', '1')->whereIn('id', $ids)->orderBy('lname', 'asc')->get();            
            return response()->json($customers);
        }
        if(auth()->user()->type == 'admin'){
            $business_owners = User::where('type', 'business_owner')->where('active', '1')->orderBy('lname', 'asc')->get();
            $customers = User::where('type', 'customer')->where('active', '1')->orderBy('lname', 'asc')->get(); 
        }else {
            $business_owners = User::find(auth()->user()->id);
            $users = User::with(['assignedBusinessOwners:users.id,users.fname,users.lname'])->find(auth()->user()->id);
            $ids = array();         
            foreach($users->assignedBusinessOwners as $own) {
                $ids[] = $own['id'];
            }
            $customers = User::where('type', 'customer')->where('active', '1')->whereIn('id', $ids)->orderBy('lname', 'asc')->get(); 
        }
        if ($request->isMethod('get')) 
        {
            $data['id'] = $id;
            $record = Reward::fetchall($data)['data'][0];
           
            $song_data['status'] = '1';
            $songs = Song::fetchall($song_data)['data'];
          
            return response()->json([
            'result' => true,
            'data' => view('rewards.edit',compact('record', 'business_owners', 'songs','customers'))->render()
            ]);
        }
        // validation
        $this->validate($request, [
            'from_business_owner' => 'required',
            'to_customer' => 'required',
            'song_id' => 'required',
            'product_link' => ['required'],
            'discount' => 'required',
            'subject' => 'required',
            'body' => 'required',
        ]); 
        
        $data = $request->all();
        unset($data["_token"]);

        if(!empty($id))
            $data["id"] = $id;
         
        if(isset($data['from_business_owner']) && !empty($data['from_business_owner'])){
            $toArr = explode('/', $data['from_business_owner']);
            $data['business_owner_id'] = $toArr[0];
            $data['from'] = $toArr[1];
        }
        if(isset($data['to_customer']) && !empty($data['to_customer'])){
            $toArr = explode('/', $data['to_customer']);
            $data['customer_id'] = $toArr[0];
            $data['to'] = $toArr[1];
        }

        $song_data['status'] = '1';
        $song_data['id'] = $data['song_id'];
        $selected_song = Song::fetchall($song_data)['data'];
        $song_name = ucwords($selected_song[0]['title']);
        $from_business_owner = User::where('type', 'business_owner')->where('active', '1')->where('id', $data['business_owner_id'])->get();

        $business_owner_name = ucwords(strtolower($from_business_owner[0]['fname'].' '.$from_business_owner[0]['lname']));

        $to_customer = User::where('type', 'customer')->where('active', '1')->where('id', $data['customer_id'])->get();

        $customer_name = ucwords(strtolower($to_customer[0]['fname'].' '.$to_customer[0]['lname']));
       
        if(isset($data['song_id']) && isset($data['customer_id']) && isset($data['discount'])){
            $param = $data['song_id'].'/'.$data['customer_id'].'/'.$data['discount'];
            $song_link =  url('/').'/play/song/'. $param;   
        }else{
            $song_link = url('/').'/play/song/';
        }

        $email_body = $data['body'];
        $email_body = str_replace("[song_name]", $song_name, $email_body);
        
        $to_email = isset($data['to']) ? $data['to'] : 'loyaltyrewardssong@gmail.com';   
        $cc = isset($data['cc']) ? $data['cc'] : '';   
        $bcc = isset($data['bcc']) ? $data['bcc'] : 'loyaltyrewardssong@gmail.com';   
        $details = [
                'template' => 'singleCustomerRewardEmail',
                'from' => $data['from'],
                'subject' => $data['subject'],
                'customer_name' => $customer_name,
                'song_link' => $song_link,
                'song_name' => $song_name,
                'product_link' => $data['product_link'],
                'discount' => $data['discount'],
                'business_owner' => $business_owner_name,
                'body' => $email_body
        ];
        //echo "<pre>"; print_r($details);die('here');
        if(!empty($cc))
            Mail::to($to_email)->cc($cc)->bcc($bcc)->send(new SendingEmail($details));
        else
            Mail::to($to_email)->bcc($bcc)->send(new SendingEmail($details));

        if(!Mail::failures())  {
            $data['email_content'] = serialize( [
                'template' => 'singleCustomerRewardEmail',
                'to' => $to_email,
                'cc' => $cc,
                'bcc' => $bcc,
                'subject' => $data['subject'],
                'body' => $email_body,
                'product_link' => $data['product_link']                
            ]);
          
            $saveRecord = Reward::saveItem($data);
            $business_owners = User::where('type', 'business_owner')->where('active', '1')->orderBy('lname', 'asc')->get();
            $songs = Song::fetchall(array('status'=>'1'))['data'];
            $records = Reward::fetchall()['data'];
            
            return response()->json([
              'result' => true,
              'data' => 'Email has been sent to the customer\'s account('.$to_email.').',
              'view' => view('rewards.edit')->with("id",$id)->with('business_owners',$business_owners)->with('songs',$songs)->with('records',$records)->with('customers',$customers)->render()
            ]);
        }          
        return response()->json([
            'result' => false,
            'data' => 'Sorry! Please try again to send an Email.'
        ]);
       
    }
   
    /**
     * Method: Find the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array JsonResponse
     */
    public function find(Request $request)
    {
        $data = $request->all();
        $result = Reward::fetchall($data);
        if($result['result'])
        {
            return response()->json([
                'result' => true,
                'query' => $result['query'],
                'data' => view('rewards.list')->with('records',$result['data'])->render()
            ]);
        }else{
            return $result;
        }
        
    }

    
    /**
     * Method: Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array JsonResponse
     */
    public function delete($id = null)
    {
        $record = Reward::find($id);
        $record->delete();
        
		return response()->json([ 
            'result' => true,
            'data' => "Record has been deleted successfully!"
        ]);
    }

    /**
     * Method: Find the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array JsonResponse
     */
    public function view($id = "")
    {
        if(!empty($id))
        {
            $record = Reward::find($id);
            $song = Song::find($record->song_id);
            $user = User::find($record->customer_id);
            return view('rewards.reportView',compact('id','record','song','user'));
        }
    }
}
