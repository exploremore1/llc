<?php

namespace App\Http\Controllers;
use App\Models\Song;
use App\Models\Reward;
use App\Models\SongPlayed;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


use Illuminate\Support\Facades\Storage;
use App\File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use PDF;

class SongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->type == 'admin'){
            $result = Song::fetchall();
        }else{
            $result = Song::fetchall(array('status'=>'1'));
        }
        if($result['result'])
        {
            $records = $result['data'];
            $host = request()->getHttpHost();
            return view('songs.index',compact('records','host'));
        }else{
            return $result;
        }
    }

    /**
     * Method: View Pdf report corresponding to specific patient's report
     *
     * @param  int  $id
     * @return array JsonResponse
     */
    public function viewPdf($id = "")
    {
        $filePath = ""; 
        if(!empty($id))
        {
            $item = Song::find($id);
            if(!empty($item['appericiation_letter']))
            {
                $filePath ='images/pdf/appericiation_letter/'.$item->appericiation_letter;
            }
            return view('songs.reportView',compact('filePath','item'));
        }
    }

     
    /**
     * Play a song from the link
     ** @param  int  $id (song_id)
     ** @param  int  $cust_id (customer_id)
     * @return \Illuminate\Http\Response
     */
    public function playSong($id="", $record_id= "")
    { 
        $filePath = ""; 
        if(!empty($id)){
            $song = Song::findOrFail($id);
            if(!empty($song) && !empty($record_id)){
                $record = Reward::findOrFail($record_id);  
                $data['song_id'] = $id;
                $customer_id = $data['customer_id'] = $record["customer_id"];
                $discount = $data['discount'] = $record["discount"];
                $product_link = $record["product_link"];
                $record = SongPlayed::saveItem($data);
                if(!empty($song['appericiation_letter']))
                {
                    $filePath ='images/pdf/appericiation_letter/'.$song->appericiation_letter;
                }
                
                // echo "<pre>";
                // print_r($filePath);
                // die;
                return view('songs.playSong',compact('song','customer_id','discount','product_link','filePath'));
            }
            elseif(!empty($song) && !empty($song['appericiation_letter']))
            {
                $filePath ='images/pdf/appericiation_letter/'.$song->appericiation_letter;
                return view('songs.playSong',compact('song','filePath'));
                
            }
            else
                return view('songs.playSong',compact('song'));

        }
        return redirect('/rewards');
    }
    
 /**
     * Method: Add/Edit 
     *
     * @param  int  $id
     * @return array JsonResponse
     */
    public function edit(Request $request,$id = "")
    {
        if ($request->isMethod('get')) 
        {
            $item = Song::findOrFail($id);
            return response()->json([
            'result' => true,
            'data' => view('songs.edit')->with("id",$id)->with('item',$item)->render()
            ]);
        }
        // validation
        $this->validate($request, [
            'title' => 'required',
            'desc' => 'nullable',
            'appericiation_letter' => 'nullable|file|mimes:pdf|max:10000',
            'thumbnail' => 'nullable|file|mimes:jpeg,jpg,png,gif|max:2048',
            'audio' =>'nullable|file|mimes:audio/mpeg,mpga,mp3,wav,aac'
        ]); 
        $data = $request->all();
        
        // handle appericiation_letter as pdf
        if($request->file('appericiation_letter'))
        {
            $appericiation_letter = $request->file('appericiation_letter');
            $appericiation_letter_name = $data["title"]."_".time().'.pdf';
            $destinationPathLetter = public_path('/images/pdf/appericiation_letter');
            
            if (!is_dir($destinationPathLetter)) {
                mkdir($destinationPathLetter, 0777, true);
            }
            $appericiation_letter->move($destinationPathLetter, $appericiation_letter_name);
            $data["appericiation_letter"] = $appericiation_letter_name;
        }
        
        if($request->file('thumbnail'))
        {
            $thumbnail = $request->file('thumbnail');
            $thumbnail_name = time().'.'.$thumbnail->getClientOriginalExtension();
            $destinationPathThumbnail = public_path('/images/thumbnail');
            if (!is_dir($destinationPathThumbnail)) {
                mkdir($destinationPathThumbnail, 0777, true);
            }
            $thumbnail->move($destinationPathThumbnail, $thumbnail_name);
            $data["thumbnail"] = $thumbnail_name;
        }
        
        // handle audio file 
        if($request->hasFile('audio')){
            $uniqueid = uniqid();
            $audio = $request->file('audio');
            $original_name = $audio->getClientOriginalName();
            // $size = $audio->getSize();
            $extension = $audio->getClientOriginalExtension();
            $audiofilename = Carbon::now()->format('Ymd').'_'.$uniqueid.'.'.$extension;
            $destinationPathAudio = public_path('/images/audio');
            if (!is_dir($destinationPathAudio)) {
                mkdir($destinationPathAudio, 0777, true);
            }
            $audio->move($destinationPathAudio, $audiofilename);
            $data["audio"] = $audiofilename;
           }

        unset($data["_token"]);

        if(!empty($id))
            $data["id"] = $id;

        $record = Song::saveItem($data);

        return response()->json([ 
                'result' => true,
                'view' => view("songs.edit")->render(),
                'action' => 'edit_song',
                'data' => "Record has been updated successfully."
        ]);
    }

    /**
     * Method: Find the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array JsonResponse
     */
    public function find(Request $request)
    {
        $data = $request->all();
        
        if(Auth::user()->type == 'admin'){
            $result = Song::fetchall($data);
        }else{
            $data['status'] = '1';
            $result = Song::fetchall($data);
        }
        if($result['result'])
        {
            return response()->json([
                'result' => true,
                'query' => $result['query'],
                'data' => view('songs.list')->with('records',$result['data'])->render()
            ]);
        }else{
            return $result;
        }
    }

    
    /**
     * Method: Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array JsonResponse
     */
    public function delete($id = null)
    {
        $record = Song::find($id);
        $record->delete();
        
		return response()->json([ 
            'result' => true,
            'data' => "Record has been deleted successfully!"
        ]);
    }

     /**
     * Method: Find the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array JsonResponse
     */
    public function view($id = "")
    {
        if(!empty($id))
        {
            $song = Song::find($id);
            return view('songs.reportViewDetails',compact('id','song'));
        }
    }
}
