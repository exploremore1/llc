<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\HeadingRowImport;
use App\Imports\UsersImport;

use App\Models\User;
use App\Models\Song;
use App\Models\Reward;

use App\Mail\SendingEmail;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->type == 'business_owner'){
            $users = $records = User::fetchall()['data'];
            return view('customers.index',compact('users', 'records'));
        }else{
            $users = User::where('type', 'customer')->orderBy('lname', 'asc')->get();
            $records = User::fetchall()['data'];
            $business_owners = User::where('type', 'business_owner')->where('active', '1')->orderBy('lname', 'asc')->get();
            return view('customers.index',compact('users', 'records', 'business_owners'));
        }
    }
    
    /**
     * Method: Add/Edit User data and password based on Type param
     *
     * @param  int  $id
     * @param  string  $type
     * @return array JsonResponse
     */
    public function edit(Request $request, $id = "", $type = "")
    {
        $business_owners = User::where('type', 'business_owner')->where('active', '1')->orderBy('lname', 'asc')->get();
        
        if ($request->isMethod('get')) 
        {            
            $users = User::with(['assignedCustomers:users.id,users.fname,users.lname'])->find($id);
            $ids = array();         
            foreach($users->assignedCustomers as $own) {
                $ids[] = $own['id'];
            }
            $users->business_owners = $ids;
            $view = (!empty($type) && ($type == "password"))?"customers.editpassword" :"customers.edit";
            
            return response()->json([
                'result' => true,
                'data' => view($view)->with("id",$id)->with('users',$users)->with('business_owners',$business_owners)->render()
                ]);
        }
        
        $data = $request->all();
        $data['type'] = 'customer';
        $data['task'] = 'save_customer';
        if(empty($type) ) {
            if(!empty($id))
            {
                $validator = Validator::make($data, [
                    'email' => ['required',Rule::unique('users')->ignore($id)],
                    ])->validate();   
            }else{               
                $validator = Validator::make($data, [
                    'email' => 'required|unique:users',
                    ])->validate();   
            }
        }

        unset($data["_token"]);
        if(!empty($id)){
            $data["id"] = $id;
            
            //Update Password          
            if(!empty($type) && ($type == "password"))
            {              
                if($data["password_confirmation"] != $data["password"])
                {
                    $error_msg = "Password and confirm password should be same";
                }               
                
                if(!empty($error_msg))
                {
                    return response()->json([ 'result' => false,
                        'data' => $error_msg
                    ]);
                }

                unset($data["password_confirmation"]);
                User::saveUser($data);
                
                return response()->json([ 'result' => true,
                                      'view' => view("customers.edit")->with('business_owners',$business_owners)->render(),
                                      'action' => 'change_password',
                                      'user_type' => auth()->user()->type,
                                      'data' => "Password has been updated successfully."
                ]);                
            }
        }
        
        User::saveUser($data,$type);
        $users = User::find(auth()->user()->id);
        $business_owners = User::where('type', 'business_owner')->where('active', '1')->orderBy('lname', 'asc')->get();
          
        $view = (!empty($type) && ($type == "password"))?"customers.editpassword" :((!empty($type) && ($type == "profile"))?"customers.profileview" :"customers.edit");

        return response()->json([  'result' => true,
                                    'action' => 'edit_profile',
                                    'view' => view($view)->with('users',$users)->with('business_owners',$business_owners)->render(),
                                    'data' => "Your profile has been updated successfully."
        ]);
        
        
    }

    /**
     * Method: Send Rewards via Email to a specified customer and save in db
     *
     * @param  int  $id
     * @return array JsonResponse
     */
    public function sendEmail(Request $request,$id = "")
    {
        if($request->isMethod('get')) 
        {
            $customer = User::find($id);
            if(auth()->user()->type == 'business_owner'){
                $business_owners = User::find(auth()->user()->id);
            }else{
                $users = User::with(['assignedCustomers:users.id,users.fname,users.lname'])->find($id);
                $ids = array();         
                foreach($users->assignedCustomers as $own) {
                    $ids[] = $own['id'];
                }
                $business_owners = User::where('type', 'business_owner')->where('active', '1')->whereIn('id', $ids)->orderBy('lname', 'asc')->get();
            }
            
            $sngData['status'] = '1';
            $songs = Song::fetchall($sngData)['data'];
           
            return response()->json([
                'result' => true,
                'id' => $id,
                'id' => $customer,
                'data' => view('customers.sendEmail')->with("id",$id)->with('customer',$customer)->with('business_owners',$business_owners)->with('songs',$songs)->render()
                ]);
        } 
            
        $data = $request->all();
        unset($data["_token"]);

        $validator = Validator::make($data, [
            'to' => ['required'],
            'business_owner_id' => ['required'],
            'song_id' => ['required'],
            'discount' => ['required'],
        ])->validate();   
              
        $to_email = isset($data['to']) ? $data['to'] : 'loyaltyrewardssong@gmail.com';   
        $cc = isset($data['cc']) ? $data['cc'] : '';   
        $bcc = isset($data['bcc']) ? $data['bcc'] : 'loyaltyrewardssong@gmail.com';  

        $song_data['status'] = '1';
        $song_data['id'] = $data['song_id'];
        $selected_song = Song::fetchall($song_data)['data'];
        $from_business_owner = User::where('type', 'business_owner')->where('active', '1')->where('id', $data['business_owner_id'])->get();

        $business_owner_name = ucwords(strtolower($from_business_owner[0]['fname'].' '.$from_business_owner[0]['lname']));

        //$data['body'] = str_replace("[customer_name]", $selected_song[0]['link'], $data['body']);
        $data['body'] = str_replace("[song_link]", $selected_song[0]['link'], $data['body']);
        $data['body'] = str_replace("[business_owner]", $business_owner_name, $data['body']);

        $details = [
                    'template' => 'singleCustomerRewardEmail',
                    'subject' => $data['subject'],
                    'body' => nl2br($data['body']),
                   ];
        if(!empty($cc))
            Mail::to($to_email)->cc($cc)->bcc($bcc)->send(new SendingEmail($details));
        else
            Mail::to($to_email)->bcc($bcc)->send(new SendingEmail($details));

        if(!Mail::failures()) 
        {
            //save rewards data in rewards table
            $data['email_content'] = serialize( [
                'template' => 'singleCustomerRewardEmail',
                'to' => $to_email,
                'cc' => $cc,
                'bcc' => $bcc,
                'subject' => addslashes($data['subject']),
                'body' => addslashes($data['body'])                
            ]);
           
            $record = Reward::saveItem($data);
          
            $customer = User::find($id);
            $business_owners = User::where('type', 'business_owner')->where('active', '1')->orderBy('lname', 'asc')->get();
            $data['status'] = '1';
            $users = User::fetchall($data)['data'];
            $records = User::fetchall()['data'];

            return response()->json([
              'result' => true,
              'data' => 'Email has been sent to the customer\'s account('.$to_email.').',
              'view' => view('customers.edit')->with("id",$id)->with('customer',$customer)->with('business_owners',$business_owners)->with('users',$users)->with('records',$records)->render()
            ]);
        }  
        else
        {
            //if email didnt go, delete the last reward entry
            return response()->json([
                'result' => false,
                'data' => 'Sorry! Please try again to send an Email.'
            ]);
        }        
    }

    /**
     * Method:Send Rewards to the selected customers and save in db
     *
     * @param  int  $id
     * @return array JsonResponse
     */
    public function sendBulkEmail(Request $request, $type="")
    {   
        $current_business_owner_id = auth()->user()->id;
        $business_owner = User::where('type', 'business_owner')->where('active', '1')->where('id', $current_business_owner_id)->get();
        if($type == "viewBulkEmailTemplate") 
        {
            $data = $request->all();
            $customer_ids = implode(',',$data['customer_ids']);
            
            $sngData['status'] = '1';
            $songs = Song::fetchall($sngData)['data'];
            
            return response()->json([
                'result' => true,
                'data' => view('customers.sendBulkEmail')->with('customer_ids',$customer_ids)->with('songs',$songs)->render()
                ]);
            }
            
            $data = $request->all();
            unset($data["_token"]);
            $validator = Validator::make($data, [
                'song_id' => ['required'],
                'product_link' => ['required'],
                'discount' => ['required'],
                'body' => ['required'],
                'subject' => ['required'],
                ])->validate();   
                
                $song_data['status'] = '1';
                $song_data['id'] = $data['song_id'];
                $selected_song = Song::fetchall($song_data)['data'];
                $song_name = ucwords($selected_song[0]['title']);
        $business_owner_name = ucwords(strtolower($business_owner[0]['fname'].' '.$business_owner[0]['lname']));

        $error = false;
        $error_customer = '';
        $customer_ids = explode(',', $data['customer_ids']);
        foreach($customer_ids as $cust_id)
        {       
            
            $current_customer = User::where('type', 'customer')->where('active', '1')->where('id', $cust_id)->get();
            $to_customer = isset($current_customer[0]['fname']) ? ucwords($current_customer[0]['fname']) : 'Customer'; 
            $to_email = isset($current_customer[0]['email']) ? $current_customer[0]['email'] : 'loyaltyrewardssong@gmail.com'; 
            $bcc = 'loyaltyrewardssong@gmail.com';  
            $email_body = $data['body'];
            $email_body = str_replace("[song_name]", $song_name, $email_body);
            
            //save rewards data in rewards table
            $data['customer_id'] = $cust_id;
            $data['business_owner_id'] = $current_business_owner_id;
            $data['email_content'] = serialize( [
                'template' => 'bulkCustomerRewardEmail',
                'to' => $to_email,
                'cc' => '',
                'bcc' => $bcc,
                'subject' => addslashes($data['subject']),
                'body' => addslashes($email_body),
                'product_link' => $data['product_link'],                 
                ]); 
            $record = Reward::saveItem($data); 
            $record_id = $record["item_id"];
               
            //Creating link where the email button will redirect
            if(isset($data['song_id']) && isset($cust_id) && isset($data['discount'])){
                $song_link =  url('/').'/play/song/'. $data['song_id'].'/'.$record_id ;   
            }else{
                $song_link = url('/').'/play/song/';
            }

            //Preparing details to send it to mail
            $details = [
                'template' => 'bulkCustomerRewardEmail',
                'subject' => $data['subject'],
                'customer_name' => $to_customer,
                'song_link' => $song_link,
                'song_name' => $song_name,
                'product_link' => $data['product_link'],
                'discount' => $data['discount'],
                'business_owner' => $business_owner_name,
                'body' => $email_body,
            ];

            Mail::to($to_email)->bcc($bcc)->send(new SendingEmail($details));
            
            if(Mail::failures()){
                $error = true;
                $error_customer .=  "err ".$customer['email']; 
                //delete rewards entry     
                Reward::where('id', $record_id)->delete();
            }
        }

        if($error){
            return response()->json([
                'result' => true,
                'data' => 'Email has not been sent to the customer\'s account('.$error_customer.').',
            ]);
        }        

        return response()->json([
            'result' => true,
            'data' => 'Email has been sent to selected Customers.'
        ]);
        
    }


    /***
     * 
     * Test Email Method
     */

    public function sendSampleEmail(Request $request, $type=""){
       
        $details = [
            'template' => 'sampleTestingEmail',
            'name' => 'John Doe',
        ];
        // Test mail.
        try {
            Mail::to('loyaltyrewardssong@gmail.com')->send(new SendingEmail($details));
            echo 'Mail sent successfully';
        } catch (\Exception $e) {
            echo "<pre>";
            echo 'Error - '.$e;
        }
    
    }
    /**
     * Method: Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array JsonResponse
     */
    public function delete($id = null)
    {
        $ticket = User::find($id);
        $ticket->delete();
        return response()->json([ 
            'result' => true,
            'data' => "Record has been deleted!"
        ]);
    }

    /**
     * Method: Find the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array JsonResponse
     */
    public function find(Request $request)
    {
        $data = $request->all();
        
        $result = User::fetchall($data);
        if($result['result'])
        {
            return response()->json([
                'result' => true,
                'data' => view('customers.list')->with('users',$result['data'])->render()
            ]);
        }else{
            return $result;
        }
    }

     
    /**
     * Method: Import Mulitple customers
     *
     * @return array JsonResponse
     */
    public function bulkImport(Request $request)
    {
        if ($request->isMethod('get')) 
        {
            return response()->json([
              'result' => true,
              'data' => view('customers.bulkImport')->render()
            ]);
        } 
        $data = $request->all();
        unset($data["_token"]);
        $files = $request->file('file');
        
        if (!is_array($files)) {
            $files = [$files];
        }
        $destinationPath = public_path('/images/bulk_customers_import/');
        if (!is_dir($destinationPath)) {
            mkdir($destinationPath, 0777, true);
        }
        for ($i = 0; $i < count($files); $i++) 
        {
            $file = $files[$i];
            $name = 'customers'.time();
            $save_name = $name . '.' . $file->getClientOriginalExtension();

            $file->move($destinationPath, $save_name);

            //Check format of all headings
            $allHeading = array("fname","lname", "gender","contact","plan","active","email","password");
            $headings = (new HeadingRowImport)->toArray($destinationPath.$save_name)[0][0];
            foreach($headings as $heading)
            {
                if(!in_array($heading,$allHeading))
                {
                    return response()->json([
                        'result' => false,
                        'new' =>$heading,
                        'message' => 'Heading mismatch.Please re-check format of uploaded file. Invalid List of Heading are as follow '.$heading
                    ]);
                }
            }

            $result = Excel::import(new UsersImport(),$destinationPath.$save_name);
            unlink($destinationPath.$save_name);
        }
        return response()->json([
            'result' => true,
            'message' => 'Customers have been uploaded Successfully'
        ]);
    }
 
     /**
     * Method: Download Sample File
     *
     */
    public function downloadSampleFileBulkImport(Request $request)
    {
        if ($request->isMethod('get')) 
        {
            //create directory if not exists
            $destinationPath = public_path('images/sampleFile/');
            if (!is_dir($destinationPath)) {
                mkdir($destinationPath, 0777, true);
            }
            $path = $destinationPath.'user.xls';
            return response()->download($path);
        }
    }

     /**
     * Method: Find the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array JsonResponse
     */
    public function view($id = "")
    {
        if(!empty($id))
        {
            $customer = User::find($id);
            return view('customers.reportViewDetails',compact('id','customer'));
        }
    }
}
