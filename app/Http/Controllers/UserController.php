<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('type', 'admin')->orderBy('lname', 'asc')->get();
        return view('users.index',compact('users'));
    }

    /**
     * Method: Display a specific record
     *
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        $users = User::find(auth()->user()->id);
         if ($request->ajax()) 
        {
            return response()->json([
              'data' => view('users.profileview')->with("id",auth()->user()->id)->with('users',$users)->render()
           ]);  
        }
        return view('users.profile',compact('users'))->with("id",auth()->user()->id)->with('users',$users);
    }

    /**
     * Method: Uplaod profile picture
     *
     * @param  int  $id
     * @return null
     */
    public function uploadAvatars(Request $request,$id = "")
    {
        if($request->file('avatar'))
        {
            $image = $request->file('avatar');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/avatars');
            $image->move($destinationPath, $name);
            $data["image"] = $name;
        }

        unset($data["_token"]);
        if(!empty($id))
            $data["id"] = $id;

        User::saveUser($data);
        
        return redirect('/users/profile');
    }

    /**
     * Method: Add/Edit User data and password based on Type param
     *
     * @param  int  $id
     * @param  string  $type
     * @return array JsonResponse
     */
    public function edit(Request $request, $id = "", $type = "")
    {
        if ($request->isMethod('get')) 
        {
            $users = User::find($id);            
            $view = (!empty($type) && ($type == "password"))?"users.editpassword" :"users.edit";
            
            return response()->json([
                'result' => true,
                'data' => view($view)->with("id",$id)->with('users',$users)->render()
                ]);
        }
        
        $data = $request->all();
        $data['type'] = 'admin';
        $data['task'] = 'save_admin';

        if(empty($type) ) {
            if(!empty($id))
            {
                $validator = Validator::make($data, [
                    'email' => ['required',Rule::unique('users')->ignore($id)],
                    ])->validate();   
            }else{               
                $validator = Validator::make($data, [
                    'email' => 'required|unique:users',
                    ])->validate();   
            }
        }

        unset($data["_token"]);
        if(!empty($id)){
            $data["id"] = $id;
            
            //Update Password
            if(!empty($type) && ($type == "password"))
            {
                if($data["old_password"] == $data["password"])
                {
                    $error_msg = "New password should be different from old password";
                }
                elseif($data["password_confirmation"] != $data["password"])
                {
                    $error_msg = "Password and confirm password should be same";
                }
                $users = User::find($id);
                if (!Hash::check($request->get('old_password'), $users["password"])) 
                {
                    $error_msg = "Current password is not correct.Please try again!";
                }
                
                if(!empty($error_msg))
                {
                    return response()->json([ 'result' => false,
                        'data' => $error_msg
                    ]);
                }

                unset($data["old_password"]);
                unset($data["password_confirmation"]);
                User::saveUser($data);
                
                return response()->json([ 'result' => true,
                                      'view' => view("users.profileview")->with('users',$users)->render(),
                                      'action' => 'change_password',
                                      'data' => "Password has been updated successfully. Please login again."
                ]);
                
            }
        }
        
        User::saveUser($data,$type);
        $users = User::find(auth()->user()->id);
            
       // $view = (!empty($type) && ($type == "password"))?"users.editpassword" :((!empty($type) && ($type == "profile"))?"users.profileview" :"users.edit");
       $view = "users.profileview";
        return response()->json([  'result' => true,
                                    'action' => 'edit_profile',
                                    'view' => view($view)->with('users',$users)->render(),
                                    'data' => "Your profile has been updated successfully."
        ]);       
        
    }

    /**
     * Method: Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return array JsonResponse
     */
    public function delete($id = null)
    {
        $ticket = User::find($id);
        $ticket->delete();
        return response()->json([ 
            'result' => true,
            'data' => "Record has been deleted!"
        ]);
    }
    
    /**
     * Method: Find the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array JsonResponse
     */
    public function find(Request $request)
    {
        $data = $request->all();
        
        $users = User::findby($data)->where('type', 'admin');
        return response()->json([
            'result' => true,
            'data' => view('users.list')->with('users',$users)->render()
        ]);
    }

     /**
     * Method: Find the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array JsonResponse
     */
    public function view($id = "")
    {
        if(!empty($id))
        {
            $user = User::find($id);
            // echo "<pre>";
            // print_r($user);
            // die;
            return view('users.reportViewDetails',compact('id','user'));
        }
    }
}
