<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use DB;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'lname','email','password','type','active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    /**
     * Function: Create Many to Many Relation between
     * Business Owners and Customers     * 
     */
    public function businessOwner()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Function: Create Many to Many Relation between
     * Business Owners and Customers     * 
     */

    public function customer()
    {
        return $this->hasMany(User::class);
    }

     /**
     * Function: Get Customers associated with business owners 
     */
    public function assignedCustomers()
    {
        return $this->belongsToMany(User::class, 'customer_user',  'customer_id','user_id');
    }

    /**
     * Function: Get Business Owners associated with customers  
     */
    public function assignedBusinessOwners()
    {
        return $this->belongsToMany(User::class, 'customer_user','user_id', 'customer_id');
    }
   
    /**
     * Function: Filter User's data
     * Input : array
     * Output : array
     */
    public function Scopefetchall($query,$data = array())
    {
        DB::enableQueryLog();
        $result = array();
       
        try {
            $query->select("users.*");
            // if(!empty($data['type'])){
            //     $query->where('type', '=', $data['type']);
            // }
            if(auth()->user()->type == 'business_owner')
            { 
                $ids = array();
                $query = User::with('assignedBusinessOwners:users.id,users.fname,users.lname');
                $query->where('id', auth()->user()->id);
                $query->where('active', '1');
                $results = $query->get();
                foreach($results as $result)
                {
                    foreach($result->assignedBusinessOwners as $own)
                    {
                        $ids[] = $own['id'];
                    }
                }
                if(isset($data['filter_user']))
                {
                    $query = User::where('id', $data['filter_user']);
                }else{
                    $query = User::whereIn('id', $ids);
                }
            }else{ 
                if(isset($data['filter_business_owner']))
                {
                    $ids = array();
                    $query = User::with('assignedBusinessOwners:users.id,users.fname,users.lname');
                    $query->where('id', $data['filter_business_owner']);
                    $results = $query->get(); 
                    foreach($results as $result)
                    {
                        foreach($result->assignedBusinessOwners as $own)
                        {
                            $ids[] = $own['id'];
                        }
                    }
                    $query = User::whereIn('id', $ids);
                }else{
                    $query = User::where('type', 'customer');
                }
                
                if(isset($data['filter_user']))
                {
                    $query->where('id', $data['filter_user']);
                }
            }

            if(isset($data['value']))
            {                
                $query->where(function($query) use ($data){
                    $query->where('fname', 'like', '%' . $data['value'] . '%');
                    $query->orWhere('lname', 'like', '%' . $data['value'] . '%');
                    $query->orWhere('email', 'like', '%' . $data['value'] . '%');
                });
            }   
            $result = $query->orderBy('lname', 'asc')->get();

            $query = DB::getQueryLog();
            // echo "<pre>";  print_r($query);die;
            $result = array("result" => true,"data" => $result);
        } catch(QueryException $ex){ 
            $result = array("result" => false,"data" => "Error =>".$ex->getMessage());
        }
        return $result;
    }

    /**
     * Function: Insert/Update User's data
     * Input : array
     * Output : array
     */
    public function ScopesaveUser($query,$data)
    {
        if(isset($data['business_owners_ids']) && (auth()->user()->type == "admin") && ($data['task'] == "save_customer"))
        {
            $business_owners_ids = $data['business_owners_ids'];
            unset($data['business_owners_ids']);
        }else if( auth()->user()->type == "business_owner" && ($data['task'] == "save_customer")){
            $business_owners_ids = auth()->user()->id;
        }

        if(isset($data['id']))
        {
            $ticket = $this->find($data['id']);
            //$ticket->user_id = auth()->user()->id;
            if(isset($data['image']))
            $ticket->image = $data['image'];

            if(isset($data['fname']))
            $ticket->fname = $data['fname'];

            if(isset($data['lname']))
            $ticket->lname = $data['lname'];

            if(isset($data['gender']))
            $ticket->gender = $data['gender'];

            if(isset($data['email']))
            $ticket->email = $data['email'];

            if(isset($data['password']) && !empty(($data['password'])))
            $ticket->password =  bcrypt($data['password']);

            // if(isset($data['type']))
            // $ticket->type = $data['type'];
           
            if(isset($data['contact']))
                $ticket->contact = $data['contact'];

            if(isset($data['active']))
                $ticket->active = $data['active'];

            if(auth()->user()->type == 'businesss_owner')   {
                if(isset($data['organization']))
                $ticket->organization = $data['organization'];
            }
            if(auth()->user()->type == 'customer')   {
                if(isset($data['plan']))
                $ticket->plan = $data['plan'];
            }

            //$ticket->remember_token = $data['remember_token'];

            $ticket->save();

            if(isset($business_owners_ids) && ($data['task'] == "save_customer") && auth()->user()->type != 'customer')
                $ticket->assignedCustomers()->sync($business_owners_ids);


            return true;
            
        }else{
            //$this->user_id = auth()->user()->id;
            $this->fname = $data['fname'];
            $this->lname = $data['lname'];
            $this->email = $data['email'];
            $this->password = bcrypt($data['password']);
            $this->type = isset($data['type'])?$data['type']:'customer';
            $this->gender = isset($data['gender'])? $data['gender']:'male';
            $this->active = isset($data['active'])?$data['active']:1;
            $this->contact = isset($data['contact']) ? $data['contact']:'';

            if(auth()->user()->type == 'businesss_owner')            
                $this->organization = isset($data['organization']) ? $data['organization']:'';
            
            if(auth()->user()->type == 'customer')  
                $this->plan = isset($data['plan']) ? $data['plan']:'';
            //$this->remember_token = $data['remember_token'];
            $this->save();

            if(isset($business_owners_ids) && ($data['task'] == "save_customer") && auth()->user()->type != 'customer')
                $this->assignedCustomers()->sync($business_owners_ids);

            return true;
        }

    }

    /**
     * Function: Filter User's data
     * Input : array
     * Output : array
     */
    public function Scopefindby($query,$data)
    {
        $query->select("users.*");

        // Below query will be like select * from users where type = '%v%' and (fname like '%v% or email like '%v%)
        if(!empty($data['type'])){
            $query->where('type', '=', $data['type']);
        }
        if(!empty($data['value'])){
            $query->where(function($query) use ($data)
            {
                $query->where('fname', 'like', '%' . $data['value'] . '%')
                      ->orwhere('email','like', '%' . $data['value'] . '%');
            });
        }
        
        return $query->get();
    }
}
