<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Dbconnection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class Song extends Dbconnection
{
    use HasFactory;
    protected $table = 'songs';
    public $timestamps = true;

    protected $fillable = [
        'title',
        'link',
        'desc',
        'appericiation_letter',
        'thumbnail',
        'audio',
        'status',
    ];

     /**
     * Function: Filter Song's data
     * Input : array
     * Output : array
     */
    public function Scopefetchall($query,$data = array())
    {   
        DB::enableQueryLog();
        $result = array();
        try {
            $query = Song::orderBy('title', 'asc');
            if(isset($data['value']))
            {
                $query->where('title', 'like', '%' . $data['value'] . '%');
            }
            if(isset($data['status']))
            {
                $query->where('status',$data['status']);
            }
            if(isset($data['id']))
            {
                $query->where('id',$data['id']);
            }
            
            $result = $query->get();
            $query = DB::getQueryLog();
            
            $result = array("result" => true, "query" => $query, "data" => $result,);
        } catch(QueryException $ex){ 
            $result = array("result" => false,"data" => "Error =>".$ex->getMessage());
        }
        return $result;
    }
 
     
    /**
     * Function: Insert/Update Song's data
     * Input : array
     * Output : array
     */
    public function ScopesaveItem($query,$data)
    {   
        return $this->saveRecord($data);
    }

    
}
