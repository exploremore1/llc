<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Dbconnection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class SongPlayed extends Dbconnection
{
    use HasFactory;
    protected $table = 'songs_played';
    public $timestamps = true;

    protected $fillable = [ 'song_id', 'customer_id', 'discount'];

     /**
     * Function: Filter Song's data
     * Input : array
     * Output : array
     */
    public function Scopefetchall($query,$data = array())
    {   
        DB::enableQueryLog();
        $result = array();
        try {
            $query = Song::orderBy('updated_at', 'asc');
            if(isset($data['id']))
            {
                $query->where('id',$data['id']);
            }
            
            $result = $query->get();
            $query = DB::getQueryLog();
            
            $result = array("result" => true, "query" => $query, "data" => $result,);
        } catch(QueryException $ex){ 
            $result = array("result" => false,"data" => "Error =>".$ex->getMessage());
        }
        return $result;
    }
 
     
    /**
     * Function: Insert/Update Songs Played data
     * Input : array
     * Output : array
     */
    public function ScopesaveItem($query,$data)
    {   
        return $this->saveRecord($data);
    }

    
}
