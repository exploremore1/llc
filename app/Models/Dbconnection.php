<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App;
use Session;
use App\Models\Songs; 

class Dbconnection extends Model {
    
    public function saveRecord($data)
    {
        $result = array();
        try { 
            if(isset($data['id']))
            {
                $item = $this->findOrFail($data['id']);
                $result = $item->update($data);
                $message = "Record has been updated successfully";
                $result = array("result" => true,"data" => $message);
                
            }else{
                $item = $this->create($data);
                $item_id = $item->id;
                $result = $item->save();
                $message = "Record has been created successfully";
                $result = array("result" => true,"data" => $message , "item_id" => $item_id);
            }
            
        } catch(QueryException $ex){ 
            $result = array("result" => false,"data" => "Error =>".$ex->getMessage());
        }
        
        return $result;
    }

}