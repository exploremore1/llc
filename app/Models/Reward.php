<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Dbconnection;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class Reward extends Dbconnection
{
    use HasFactory;
    protected $table = 'rewards';
    public $timestamps = true;

    protected $fillable = [ 'business_owner_id', 'customer_id', 'song_id', 'discount', 'product_link',
        'email_content',
    ];

    /**
     * Function: Filter Reward's data
     * Input : array
     * Output : array
     */
    public function Scopefetchall($query,$data = array())
    {   
        DB::enableQueryLog();
        $result = array();
        try {
            $query = DB::table('rewards AS r');
                    $query->select(DB::raw('
                    CONCAT (b.fname, " ", b.lname) as owner_name,
                    b.email as owner_email,
                    CONCAT (c.fname, " ", c.lname) as customer_name,
                    c.email as customer_email,
                    s.title as song_title,
                    s.audio as song_audio,
                    r.*
                    '));
                    $query->join('users AS b', 'b.id', '=', 'r.business_owner_id');
                    $query->join('users As c', 'c.id', '=', 'r.customer_id');
                    $query->join('songs AS s', 's.id', '=', 'r.song_id', 'LEFT OUTER');
                    $query->where('b.active', '1');
                    $query->where('c.active', '1');
                    $query->where('s.status', '1');

                    if(isset($data['id']) && !empty($data['id']))
                    {
                        $query->where('r.id',$data['id']);
                    }
                    if(isset($data['customer_id']) && !empty($data['customer_id']))
                    {
                        $query->where('r.customer_id',$data['customer_id']);
                    }
                    if(isset($data['business_owner_id']) && !empty($data['business_owner_id']))
                    {
                        $query->where('r.business_owner_id',$data['business_owner_id']);
                    }
                    if(!empty($data['value'])){
                        $query->where(function($query) use ($data)
                        {
                            $query->where('b.fname', 'like', '%' . $data['value'] . '%')
                                  ->orwhere('b.lname','like', '%' . $data['value'] . '%')
                                  ->orwhere('c.fname','like', '%' . $data['value'] . '%')
                                  ->orwhere('c.lname','like', '%' . $data['value'] . '%');
                        });
                    }
                    if(auth()->user()->type == "business_owner"){
                        $query->where('b.id',auth()->user()->id);
                    }
                    if(auth()->user()->type == "customer"){
                        $query->where('c.id',auth()->user()->id);
                    }
            $result =  $query->get();
            
            $query = DB::getQueryLog();
           //echo "<pre>"; print_r($query);print_r($result);die('here');
            $result = array("result" => true, "query" => $query, "data" => $result,);
        } catch(QueryException $ex){ 
            $result = array("result" => false,"data" => "Error =>".$ex->getMessage());
        }
        return $result;
    }
 
     
    /**
     * Function: Insert/Update reward's data
     * Input : array
     * Output : array
     */
    public function ScopesaveItem($query,$data)
    {   
        return $this->saveRecord($data);
    }

    
}
