<?php

namespace App\Imports;

use App\Models\User;

use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;

class UsersImport implements ToCollection, WithHeadingRow
{       
    use Importable;
    
    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $businessOwner_id = "";
            $validator = Validator::make($row->toArray(), [
                'email' => 'required|unique:users',
                ])->validate();   
                //find businessOwner id
                $businessOwner_id= auth()->user()->id;
                
                // $char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                // $number = '0123456789';
                // $random_number = substr(str_shuffle($char), 0, 2).substr(str_shuffle($number), 0, 7);
                
                $user = new User([
                    'fname'     => trim($row['fname']),
                    'lname'     => trim($row['lname']),
                    'gender'    => trim($row['gender']),
                    'contact'    => trim($row['contact']),
                    'plan'    => trim($row['plan']),
                    'email'     => trim($row['email']),
                    'password' => (isset($row['password']) && !empty($row['password']))?bcrypt(trim($row['password'])):bcrypt('654321'),
                    'type' => 'customer',
                    'active' => '1',
                    ]);
                    $user->save();
                    // print_r($user);die;

                    //change this
            if(isset($businessOwner_id) && !empty($businessOwner_id)){
                
                $user->assignedCustomers()->sync(array($businessOwner_id));
            }
        }
        return $user;
    }

    public function onError(\Throwable $e)
    {
        return array('data'=>'error');
    }

}
