<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendingEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $details;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        switch($this->details['template'])
        {
            case 'bulkCustomerRewardEmail':
                return $this->subject($this->details['subject'])
                            ->markdown('emails.bulkEmail')
                            ->with('details',$this->details);
                break;
                
            case 'singleCustomerRewardEmail':
                return $this->subject($this->details['subject'])
                            ->markdown('emails.rewardEmail')
                            ->with('details',$this->details);
                break;
            case 'sampleTestingEmail':
                return $this->markdown('emails.sampleEmail') 
                ->with('content',$this->details);
                break;
            default:
                return $this->subject('Here\'s Your Appreciation Reward Song!')
                ->from('admin@loyaltyrewardssong.com')
                ->view('emails.sendingEmail');
            break;
        }
    }
}
