<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('fname')->after('id')->nullable();
            $table->string('lname')->after('fname')->nullable();
            $table->string('image')->after('lname')->nullable();
            $table->enum('type', ['admin', 'business_owner', 'customer'])->after('image')->nullable();
            $table->enum('gender', ['male', 'female', 'others'])->after('type')->nullable();
            $table->string('organization')->after('gender')->nullable();
            $table->string('contact')->after('organization')->nullable();
            $table->string('plan')->after('contact')->nullable();
            $table->tinyInteger('active')->after('plan')->default('1');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
