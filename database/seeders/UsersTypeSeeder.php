<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\User;

class UsersTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "fname" => "admin",
                "lname" => "admin",
                "email" => "admin@gmail.com",
                "password" => Hash::make('admin!@#'),
                "type" => "admin",
                "gender" => "male",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "admin21",
                "lname" => "admin21",
                "email" => "admin21@gmail.com",
                "password" => Hash::make('admin!@#'),
                "type" => "admin",
                "gender" => "female",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "developer",
                "lname" => "developer",
                "email" => "developer@gmail.com",
                "password" => Hash::make('developer!@#'),
                "type" => "admin",
                "gender" => "female",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "businessowner",
                "lname" => "businessowner",
                "email" => "businessowner@gmail.com",
                "password" => Hash::make('businessowner!@#'),
                "type" => "business_owner",
                "gender" => "female",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "customer",
                "lname" => "customer",
                "email" => "customer@gmail.com",
                "password" => Hash::make('customer!@#'),
                "type" => "customer",
                "gender" => "male",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "harvey",
                "lname" => "specter",
                "email" => "harveyspecter@gmail.com",
                "password" => Hash::make('123456789'),
                "type" => "business_owner",
                "gender" => "male",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "mike",
                "lname" => "ross",
                "email" => "mikeross@gmail.com",
                "password" => Hash::make('123456789'),
                "type" => "business_owner",
                "gender" => "male",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "louis",
                "lname" => "litt",
                "email" => "louislitt@gmail.com",
                "password" => Hash::make('123456789'),
                "type" => "business_owner",
                "gender" => "male",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "donna",
                "lname" => "paulson",
                "email" => "donnapaulson@gmail.com",
                "password" => Hash::make('123456789'),
                "type" => "customer",
                "gender" => "female",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "dana",
                "lname" => "scott",
                "email" => "danascott@gmail.com",
                "password" => Hash::make('123456789'),
                "type" => "customer",
                "gender" => "female",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "travis",
                "lname" => "tanner",
                "email" => "travistanner@gmail.com",
                "password" => Hash::make('123456789'),
                "type" => "customer",
                "gender" => "male",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "rachel",
                "lname" => "zane",
                "email" => "rachelzane@gmail.com",
                "password" => Hash::make('123456789'),
                "type" => "customer",
                "gender" => "female",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "oliver",
                "lname" => "grady",
                "email" => "olivergrady@gmail.com",
                "password" => Hash::make('123456789'),
                "type" => "customer",
                "gender" => "male",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "claire",
                "lname" => "smith",
                "email" => "clairesmith@gmail.com",
                "password" => Hash::make('123456789'),
                "type" => "customer",
                "gender" => "female",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "sheila",
                "lname" => "saz",
                "email" => "sheilasaz@gmail.com",
                "password" => Hash::make('123456789'),
                "type" => "customer",
                "gender" => "female",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "esther",
                "lname" => "litt",
                "email" => "estherlitt@gmail.com",
                "password" => Hash::make('123456789'),
                "type" => "customer",
                "gender" => "female",
                "created_at" => Carbon::now()
            ],
            [
                "fname" => "katrina",
                "lname" => "bennett",
                "email" => "katrinabennett@gmail.com",
                "password" => Hash::make('123456789'),
                "type" => "customer",
                "gender" => "female",
                "created_at" => Carbon::now()
            ],
           
        ];
        User::insert($data);
    }
}
