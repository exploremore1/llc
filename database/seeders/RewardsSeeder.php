<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\Reward;

class RewardsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "title" => "song1",
                "link" => "abc",
                "desc" => "This is Song1",
                "thumbnail" => "",
                "audio" => "",
                "status" => "1",
                "created_at" => Carbon::now()
            ],
            [
                "title" => "song2",
                "link" => "xyz",
                "desc" => "This is Song2",
                "thumbnail" => "",
                "audio" => "",
                "status" => "0",
                "created_at" => Carbon::now()
            ],
            [
                "title" => "song3",
                "link" => "asd",
                "desc" => "This is Song3",
                "thumbnail" => "",
                "audio" => "",
                "status" => "0",
                "created_at" => Carbon::now()
            ],
        ];
        Reward::insert($data);
    }
}
