<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\Song;

class SongsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "title" => "song1",
                "link" => "http://abc.com",
                "desc" => "This is description for song1",
                "thumbnail" => "1619005170.png",
                "audio" => "20210428_6089625c9744b.mp3",
                "status" => "1",
                "created_at" => Carbon::now()
            ],
            [
                "title" => "song2",
                "link" => "http://xyz.com",
                "desc" => "This is description for song2",
                "thumbnail" => "1619005170.png",
                "audio" => "20210428_6089625c9744b.mp3",
                "status" => "1",
                "created_at" => Carbon::now()
            ],
            [
                "title" => "song3",
                "link" => "http://pqr.com",
                "desc" => "This is description for song3",
                "thumbnail" => "1619075372.jpg",
                "audio" => "20210428_6089625c9744b.mp3",
                "status" => "1",
                "created_at" => Carbon::now()
            ],
        ];
        Song::insert($data);
    }
}
