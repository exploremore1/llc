Setup new Database
    copy env.example to env file

Add your database name
    DB_DATABASE=LLC

Run Below commands to migrate Tables
    php artisan migrate
        or 
    php artisan migrate:fresh // Drop all tables and migrate again

 composer require maatwebsite/excel