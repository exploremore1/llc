<div class="card">
    <div class="card-body">
        <h4 class="card-title">Business Owners List</h4>
        <h6 class="card-subtitle"></h6>
        <div class="table-responsive m-t-40">
            <table id="listTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th >Action</th>
                        <!-- <th>ID</th> -->
                        <th>NAME</th>
                        <th class="hidden-xs hidden-sm">EMAIL</th>
                        <th class="hidden-xs hidden-sm">STATUS</th>
                    </tr>
                </thead>
                <tbody>
                @php ($i = 0)
                    @foreach($users as $k => $owner)
                    <tr>
                        <td >
                            @if(auth()->user()->type == 'admin')
                            <a href="{{route('business_owners.edit',['id'=>$owner->id, 'type'=>'password'])}}"
                                class="editpassword fa fa-key blue"  data-toggle="tooltip" title="Edit Password">
                            </a>
                            @endif
                            <a href="{{route('business_owners.edit',$owner->id)}}" class="edit fa fa-edit green" data-toggle="tooltip" title="Edit User">
                            </a>
                            <a href="{{route('business_owners.delete',$owner->id)}}" class="delete fa fa-trash-o red" data-toggle="tooltip" title="Delete User"></a>
                            <a href="{{route('business_owners.view',$owner->id)}}" class="blue" data-toggle="tooltip" title="View Details">
                            <i class="fa fa-eye"></i>View</a>
                        </td>
                        <!-- <td>{{++$i}}</td> -->
                        <td>{{ucwords(strtolower($owner->fname))}} {{ucwords(strtolower($owner->lname))}}</td>
                        <td class="hidden-xs hidden-sm">{{$owner->email}}</td>
                        <td class="hidden-xs hidden-sm">
                          @if($owner->active == '1')
                            <i class="badge badge-success">Active</i>
                          @elseif($owner->active == '0')
                          <i class="badge default">Inactive</i>
                          @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>