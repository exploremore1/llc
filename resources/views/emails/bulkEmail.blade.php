@component('mail::message')
# Dear {{$details['customer_name']}},

{!! nl2br($details['body']) !!}

@component('mail::button', ['url' => $details['song_link']])
CLICK HERE TO LISTEN TO
"{{$details['song_name']}}"
@endcomponent


Thanks,<br>
{{ $details['business_owner'] }}<br>
{{ config('app.name') }}
@endcomponent