<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-label">Home</li> 
                <li> <a class="" href="{{ URL::to('users/profile') }}" aria-expanded="true"><i class="fa fa-book"></i>
                    <span class="hide-menu" >Profile</span></a>
                </li>
                @if(Auth::user()->type == 'admin')
                <li> <a class="" href="{{ URL::to('users') }}" aria-expanded="true"><i class="fa fa-users"></i>
                    <span class="hide-menu" >Admin</span></a>
                </li>
                <li> <a class="" href="{{ URL::to('business-owners') }}" aria-expanded="true"><i class="fa fa-user-md"></i>
                    <span class="hide-menu" >Business Owners</span></a>
                </li>
                @endif
                
                @if(in_array(Auth::user()->type, array('admin','business_owner')))
                <li> <a class="" href="{{ URL::to('customers') }}" aria-expanded="true"><i class="fa fa-list"></i>
                    <span class="hide-menu" >Customers</span></a>
                </li>
                <li> <a class="" href="{{ URL::to('songs') }}" aria-expanded="true"><i class="fa fa-music"></i>
                    <span class="hide-menu" >Songs</span></a>
                </li>
                @endif
                <li> <a class="" href="{{ URL::to('rewards') }}" aria-expanded="true"><i class="fa fa-tachometer"></i>
                    <span class="hide-menu" >Rewards Dashboard</span></a>
                </li>
               
                <li class="nav-label">
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="fa fa-power-off"></i>
                        <span class="hide-menu" >Logout</span>
                    </a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</div>