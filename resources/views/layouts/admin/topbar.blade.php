<?php
$todays_date = new DateTime("now", new DateTimeZone('America/Los_Angeles') );
?>
<div class="header">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- Logo -->
        <div class="navbar-header topbar-left-heading">
            <a class="navbar-brand" href="{{asset('/users')}}">
                <span>
                <strong>Loyalty Reward Song </strong>
                    <!-- Logo icon -->
                    <!-- <b><img src="{{asset('images/logo.png')}}" alt="homepage" class="dark-logo" style="width:100%" /></b> -->
                    <!--End Logo icon -->
                    <!-- Logo text -->
                </span>
            </a>
        </div>
        
        <!-- End Logo -->
        <div class="navbar-collapse">
            <!-- toggle and nav items -->
            <ul class="navbar-nav mr-auto col-md-3">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted " 
                href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
            </ul>
            <span class="header-date hidden-md-up">
            <span class="iphone">{{$todays_date->format(" F d, Y H:i A")}}
                    </span>
                    
                    <span class="header-title pull-right">  Hi {{ucwords( strtolower(Auth::user()->fname))}}  </span></span>
                <div  class="date hidden-sm-down navbar-nav mr-auto col-md-3 text-align-center padding-top-15">
                    
                </div>
                <!-- User profile and search -->
            
            <div class="hidden-sm-down col-md-3 pull-right text-right topbar-right-section">
                <?php
                $imagePath = "images/no_avatars.jpg";
                $userimage = Auth::user()->image;
                if (!empty($userimage)) {
                    $imagePath = "images/avatars/" . Auth::user()->image;
                }
                ?> 
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="{{asset($imagePath)}}" alt="user" class="profile-pic" />
                        <span>  Hi {{ucwords( strtolower(Auth::user()->fname))}} </span>
                    </a>
                    <a class="nav-link sidebartoggler hidden-sm-down text-muted" 
                            href="javascript:void(0)"><i class="ti-menu"></i></a> 
                    <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                        <ul class="dropdown-user profile_right_menu">
                            <li>
                            <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                    <i class="fa fa-power-off"></i> Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                        
                    </div>
                </li>
            
        </div>
    </nav>
</div>