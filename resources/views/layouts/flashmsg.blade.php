<!-- Flash message -->
<div class="flash-message">
  
  @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <h6>
    <p class="alert alert-{{ $msg }}"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>{{ Session::get('alert-' . $msg) }} </p>
    </h6>
    @endif
  @endforeach
</div> 
<!-- end .flash-message -->