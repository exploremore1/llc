<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Loyalty Reward Song') }}</title>

        
        <!-- Styles -->
        <!-- <link rel="stylesheet" href="{{ asset('theme/bootstrap/bootstrap.min.css') }}"> -->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        
        <link href="{{asset('css/library/jquery-ui.css')}}" rel="stylesheet">
        
        <!-- DataTables -->
        <link rel="stylesheet" href="{{ asset('theme/datatables/css/dataTables.bootstrap5.min.css') }}">
        
        <!-- Fonts -->
        <link rel="stylesheet" href="{{ asset('theme/fonts/fonts') }}">
        <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap"> -->
        <link rel="stylesheet" href="{{ asset('theme/icons/font-awesome/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
        
        @livewireStyles

        <!-- Scripts -->
        <!-- <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.3/dist/alpine.js" defer></script> -->
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            @livewire('navigation-dropdown')

            <!-- Page Heading -->
            <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>

        @stack('modals')

        <!-- @livewireScripts -->

        <!-- <script src="{{asset('js/jquery.min.js')}}"></script> -->
        <script src="{{asset('js/jquery-ui.js')}}"></script>
        <!-- <script src="{{asset('theme/bootstrap/bootstrap.min.js')}}"></script> -->
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="{{asset('theme/datatables/datatables.min.js')}}"></script>
        <script src="{{asset('js/default.js')}}"></script>
        <script src="{{asset('js/user_index.js')}}"></script>
    </body>
</html>

