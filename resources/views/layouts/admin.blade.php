<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/llc.png')}}">
    <title>Loyalty Reward Song</title>

    <link href="{{asset('css/library/toastr.min.css')}}" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="{{asset('css/library/bootstrap.min.css')}}" rel="stylesheet">
      <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('css/library/bootstrap-datepicker3.min.css')}}" rel="stylesheet">
 
    <link href="{{asset('css/helper.css')}}" rel="stylesheet">
    <link href="{{asset('css/library/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/library/chosen.css')}}" rel="stylesheet">
    <link href="{{asset('css/library/jquery-ui.css')}}" rel="stylesheet">
    <link href="{{asset('css/library/dropzone.css')}}" rel="stylesheet">
   
<![endif]-->
</head>

<body class="fix-header fix-sidebar">

    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader"  id="preloader">
        <div id="status">
            <i class="fa fa-music fa-spin"></i>
        </div>
    </div>
    
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        @include('layouts.admin.topbar')
        <!-- End header header -->
        
        <!-- Left Sidebar  -->
        @include('layouts.admin.sidebar')
        <!-- End Left Sidebar  -->
        
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            @yield('content')
            <!-- footer -->
            <footer class="footer"> © 2021-2022 All rights reserved.</footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
        
    </div>
    <!-- End Wrapper -->
    
    <!-- All Jquery -->
    <script src="{{asset('js/library/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('js/library/popper.min.js')}}"></script>
    <script src="{{asset('js/library/bootstrap.min.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{asset('js/library/jquery.slimscroll.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{asset('js/library/sidebarmenu.js')}}"></script>
    <!--stickey kit -->
    <script src="{{asset('js/library/sticky-kit.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/library/jquery-ui.js')}}"></script>
    <script src="{{asset('js/library/chosen.jquery.min.js')}}"></script>
    <script src="{{asset('js/library/select2.min.js')}}"></script>
    <script src="{{asset('js/library/dropzone.js')}}"></script>
    <script src="{{asset('js/library/jquery.datetimepicker.full.js')}}"></script>
       
    <script src="{{asset('js/library/scripts.js')}}"></script>
    <script src="{{asset('js/library/default.js')}}"></script>
    <script src="{{asset('js/library/toastr.min.js')}}"></script>
    <script src="{{asset('js/library/datatables.min.js')}}"></script>

    <!-- <script src="{{asset('js/user_index.js?v=1.0.1')}}"></script>
    <script src="{{asset('js/user_profile.js?v=1.0.1')}}"></script> -->

 
     <?php
        $file = ""; 
        $action = Route::currentRouteAction();
        if(preg_match("/.{21}(?<method>.+)/",$action,$matches) === 1)
        {
             $file= strtolower(str_replace("Controller@", "_", $matches["method"]));
            ?>
                <script src="{{asset('js/'.$file.'.js')}}"></script>
            <?php
        }
      ?>
       @yield('page-js-files')
       @yield('page-js-script')
</body>

</html>