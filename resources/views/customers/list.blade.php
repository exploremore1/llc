<div class="card">
    <div class="card-body">
        <h4 class="card-title">Customers List</h4>
        <h6 class="card-subtitle">
            @if(auth()->user()->type == 'business_owner')
            <a href="{{route('customers_send_bulk_email')}}" 
               class="btn btn-primary .sendBulkEmail" data-toggle="tooltip" id="send_bulk_email_btn"
               title="Send Email">Send bulk email
            </a>
            @endif
            @if(auth()->user()->type == 'business_owner' || auth()->user()->fname == 'developer')
            <a href="{{route('customers_bulk_import')}}"
                class="btn btn-primary bulkImport" data-toggle="tooltip" title="Import Customers"><i class="fa fa-upload" aria-hidden="true"></i>
                 Import Customers
            </a>
            @endif
        </h6>
        <div class="table-responsive m-t-40">
            <table id="listTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        @if(auth()->user()->type == 'business_owner')
                        <th><input id="check_all" type="checkbox"> </th>
                        @endif
                        <th>Action</th>
                        <!-- <th>ID</th> -->
                        <th>NAME</th>
                        <th class="hidden-xs hidden-sm">EMAIL</th>
                        <th class="hidden-xs hidden-sm">STATUS</th>
                    </tr>
                </thead>
                <tbody>
                @php ($i = 0)
                    @foreach($users as $k => $customer)
                    <tr>
                        @if(auth()->user()->type == 'business_owner')
                        <td>
                            <input type="checkbox" class='row-check' name='row-check[]'  value="{{ $customer->id }}">
                        </td>
                        @endif
                        <td >
                            @if(auth()->user()->type == 'developer')
                            <a href="{{route('customer_send_email',$customer->id)}}" class="sendEmail fa fa-envelope black" data-toggle="tooltip" title="Send Email">
                            </a>
                            @endif
                            @if(auth()->user()->type == 'admin')
                            <a href="{{route('customers.edit',['id'=>$customer->id, 'type'=>'password'])}}"
                                class=" editpassword fa fa-key blue"  data-toggle="tooltip" title="Edit Password"> </a>
                            @endif
                            <a href="{{route('customers.edit',$customer->id)}}" class=" edit fa fa-edit green" data-toggle="tooltip" title="Edit User">
                            </a>
                            <a href="{{route('customers.delete',$customer->id)}}" class=" delete fa fa-trash-o red" data-toggle="tooltip" title="Delete User"></a>
                            <a href="{{route('customers.view',$customer->id)}}" class="blue" data-toggle="tooltip" title="View Details">
                            <i class="fa fa-eye"></i>View</a>
                        </td>
                        <!-- <td>{{++$i}}</td> -->
                        <td>{{ ucwords(strtolower($customer->fname))}} {{ucwords(strtolower($customer->lname))}}</td>
                        <td class="hidden-xs hidden-sm">{{$customer->email}}</td>
                        <td class="hidden-xs hidden-sm">
                          @if($customer->active == '1')
                            <i class="badge badge-success">Active</i>
                          @elseif($customer->active == '0')
                          <i class="badge default">Inactive</i>
                          @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>