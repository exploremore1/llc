@extends('layouts.admin')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles blue-bg">
    <div class="col-md-5 align-self-center">
        <h3 class="text-white"><i class="fa fa-music"></i>Customers</h3> </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Customers</li>
        </ol>
    </div>
</div>
<!-- End Bread crumb -->

<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start Report View -->
    <div class="row page-titles">
        <div class="col-md-12">
            <div class="panel-group panel-collapse-upload">
                <div class="panel panel-default">
                    <div class="panel-heading">View Details
                        <a href="{{ route('customers.list') }}" class="btn btn-primary pull-right view-back-btn"><i class="fa fa-arrow-left"></i> BACK</a>
                    </div>
                    <div class="panel-body view-panel">
                        <div class="row"> 
                            <div class="col-md-3 col-xs-12">Name:</div>
                            <div class="col-md-9 col-xs-12">{{ isset($customer->fname) ? ucwords($customer->fname) : '' }} {{ isset($customer->lname) ? ucwords($customer->lname) : '' }}</div>
                        </div>
                        
                        <div class="row"> 
                            <div class="col-md-3 col-xs-12">Email:</div>
                            <div class="col-md-9 col-xs-12"> {{ isset($customer["email"])? $customer->email :'' }}</div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-3 col-xs-12">Status:</div>
                                @if($customer->active == '1')
                                    <div class="col-md-9 col-xs-12"> <i class="badge badge-success">Active</i></div>
                                @elseif($customer->active == '0')
                                    <div class="col-md-9 col-xs-12"> <i class="badge default">Inactive</i></div>
                                @endif
                            </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <!-- End Report View -->
</div>
<!-- End Container fluid  -->
@endsection