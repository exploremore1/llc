<div class="card-title">
    <h4>Filters Customer List</h4>
</div>
<div class="card-body">
    <div class="basic-form col-md-12">
        <form method="post" action="{{url('/find/customers')}}" id="filter_form">
            <div class="form-group">
                <input type="hidden" value="{{csrf_token()}}" name="_token" />
                <div class='row expand_row'>
                    <div class='col-md-4 filterRow'>
                        <input type="text" class="form-control" name="value" placeholder="Name / Email" id="filter_title" />
                    </div>
                    
                    @if(auth()->user()->type == 'developer')
                    <div class='col-md-3 filterRow'>
                            <select name="filter_business_owner" class="form-control select2" id="filter_business_owner">
                            <option  value="" selected>Select Business Owner</option>
                            @foreach ($business_owners as $k => $owner)
                            <option value="{{ $owner->id }}">{{ ucwords(strtolower($owner->lname)) }} {{ ucwords(strtolower($owner->fname)) }}</option>
                            @endforeach
                            </select>
                        </div>
                    @endif
                    @if(auth()->user()->type == 'developer')
                    <div class='col-md-3 filterRow'>
                        <select name="filter_user" class="form-control select2" id="filter_user">
                        <option selected>Select Customer</option>
                        @foreach ($records as $k => $customer)
                        <option value="{{ $customer->id }}">{{ ucwords(strtolower($customer->lname)) }}, {{ ucwords(strtolower($customer->fname)) }}</option>
                        @endforeach
                        </select>
                    </div>
                    @endif
                    <div class='col-md-4'>
                        <button class="btn btn-primary" id="filter_form_submit">Submit</button>&nbsp;
                        <button class="btn btn-default" id="filter_form_reset">Reset</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>