<div class="row page-titles">
    <div class="col-md-12">
        <div class="panel-group panel-collapse-upload">
            <div class="panel panel-default">
                <div class="table-responsive m-t-40">
                @if(auth()->user()->fname=='developer')
                    <a href="{{ route('downloadSampleFileBulkImport')}}" target="_blank">
                        <button class="btn"><i class="fa fa-download"></i>Download Sample File</button>
                    </a>
               
                @endif
                    <p class="alert alert-warning">
                        Make sure to include all mentioned titles in a first row into the excel/csv file before import
                    </p>
                <table class="display nowrap table table-hover table-striped table-bordered" cellspacing="0"
                    width="100%">
                    <thead>
                        <tr>
                            <th>fname</th>
                            <th>lname</th>
                            <th>gender</th>
                            <th>contact</th>
                            <th>plan</th>
                            <th>active</th>
                            <th>email</th>
                            <th>password</th>
                        </tr>
                    </thead>
                    <tbody>
                </table>
                <div>
                <form action="{{ url('/bulkImport/customers/') }}" method="POST" enctype="multipart/form-data"
                    class="dropzone" id="import-dropzone">
                    {{ csrf_field() }}
                    <div class="fallback">
                        <input name="file" type="file" />
                    </div>
                    <div class="dz-default dz-message"><span class="btn btn-primary fa fa-upload">Upload</span></div>
                </form>
            </div>
        </div>
    </div>
</div>