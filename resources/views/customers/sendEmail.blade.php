<div class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#add">
                    <span class="glyphicon glyphicon-plus"></span> 
                    <strong id="title">Email Template</strong>
                </a>
            </h4>
        </div>
        <div id="add" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="card-body">
                    <div class="basic-form col-md-12">
                        <form method="post" action="{{ url('/sendEmail/customer/'.$id) }}"  id="sendEmail_form">
                        
                            <input type="hidden" name="customer_id" value="{{ isset($customer['id']) ? $customer['id']:'' }}"/>
                           
                            <div class="form-group">
                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                <label for="to">To:<span class="asterisk">*</span></label>
                                <input type="text" required class="form-control" placeholder="To" name="to" value="{{ isset($customer['email']) ? $customer['email']:'' }}"/>
                            </div>
                            @if(auth()->user()->type == "admin")
                            <div class="form-group">
                                <label for="tag">From Business Owner:</label>
                                <select name="business_owner_id" class="form-control" id="business_owners_id">
                                <option value="">Select Business Owner</option>
                                @foreach ($business_owners->all() as $k => $owner)
                                    <option value="{{ $owner->id }}"
                                    {{ ((isset($users['business_owners']) && in_array($owner->id,$users['business_owners']))?'selected':'') }}
                                    >
                                    {{ ucwords(strtolower($owner->lname)) }} {{ ucwords(strtolower($owner->fname)) }}
                                    </option>
                                @endforeach
                                </select>
                            </div>
                            @else
                            <input type="hidden" name="business_owner_id" value="{{ auth()->user()->id }}"/>
                            @endif

                            <div class="form-group">
                                <label for="cc">Cc:</label>
                                <input type="text" class="form-control" placeholder="cc" name="cc" value=""/>
                            </div>

                            <div class="form-group">
                                <label for="bcc">Bcc:</label>
                                <input type="text" class="form-control" placeholder="bcc" name="bcc" value="pallavikhokharofficial@gmail.com"/>
                            </div>

                            <div class="form-group">
                                <label for="tag">Songs:</label>
                                <select name="song_id" class="form-control " id="song_id" >
                                <option value="" >Select Song</option>
                                @foreach ($songs->all() as $k => $song)
                                    <option value="{{ $song->id }}"  >
                                    {{ ucwords(strtolower($song->title)) }}
                                    </option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tag">Discount Percentage:</label>
                                <select name="discount" class="form-control" id="discount" >
                                <option value="" >Select Discount</option>
                                <option value="10" >10%</option>
                                <option value="20" >20%</option>
                                <option value="50" >30%</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tag">Subject:</label>
                                <input type="text"  name="subject" required class="form-control" placeholder="Rewards" value="Here's Your Appreciation Reward Song!">
                            </div>
                            <div class="form-group">
                                <label for="tag">Body:</label>
                                <textarea name="body" rows="6" cols="80" class="form-control" style="height:300px">
                                
Hi {{ strtoupper($customer['fname']) }}!

I just want to thank you for being such
an amazing customer/subscriber! You have 
have shown amazing trust in (name of product)

So I am sending you  a special 
reward to show you my appreciation!

It's my special reward  to show
how much I really appreciate your 
trust! I won't mention it here
as I don't want to spoil the 
surprise. :)

Hint just visit the following link
and click on the left link.

IMPORTANT! I also have another
surprise for you on the right
side of the page.

Go here now to get your deserved
award and thank you for being
the most important part of
our success!

Click Here To get Your 
Appreciation Reward Now!
[song_link]

[business_owner]                         
                                </textarea>
                            </div>
                            
                            <button type="submit" class="btn btn-primary" id="sendEmail">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
