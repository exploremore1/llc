<div class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#add">
                    <span class="glyphicon glyphicon-plus"></span> 
                    <strong id="title">Bulk Email Template</strong>
                </a>
            </h4>
        </div>
        <div id="add" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="card-body">
                    <div class="basic-form col-md-12">
                        <form method="post" action="{{ url('/sendBulkEmail/customers/') }}"  id="sendBulkEmail_form">
                            <input type="hidden" name="customer_ids" value="{{isset($customer_ids) ? $customer_ids : ''}}">
                          
                         
                            <div class="form-group">
                                <label for="tag">Songs:<span class="asterisk">*</span></label>
                                <select name="song_id" required class="form-control " id="song_id" >
                                <option value="" >Select Song</option>
                                @foreach ($songs->all() as $k => $song)
                                    <option value="{{ $song->id }}"  >
                                    {{ ucwords(strtolower($song->title)) }}
                                    </option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tag">Product's Link:<span class="asterisk">*</span></label>
                                <input type="text"  name="product_link" required class="form-control"  value="">
                            </div>
                            <div class="form-group">
                                <label for="tag">Discount Percentage: <span class="asterisk">*</span></label>
                                <select name="discount" required class="form-control" id="discount" >
                                <option value="" >Select Discount</option>
                                <option value="10" >10%</option>
                                <option value="20" >20%</option>
                                <option value="30" >30%</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tag">Subject:<span class="asterisk">*</span></label>
                                <input type="text"  name="subject" required class="form-control" placeholder="Rewards" value="Here's Your Appreciation Reward Song!">
                            </div>
                            <div class="form-group">
                                <label for="tag">Body:<span class="asterisk">*</span></label>
                                <textarea name="body" rows="6" cols="80" required class="form-control" style="height:300px">

Greetings from Loyalty Rewards!

We would like to express our sincere appreciation for being such an astounding customer/subscriber!
You have shown amazing trust in "Loyalty Rewards Song" !!

So sending across a special reward which you deserve to show our gratitude!
But we won't unveil it here to spoil the surprise. :)
Let's keep the excitement flow and click on the below mentioned button.

Trust me you'll be flabbergasted  just by visiting the following link.
Listen to this melodious track and enjoy your reward along!!

So keep associated with us and let us recompense for our bond. 
We wish you an amazing day ahead!!


                            </textarea>
                            </div>
                            <button type="submit" class="btn btn-primary" id="sendBulkEmail">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
