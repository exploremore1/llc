<div class="card">
    <div class="card-body">
        <h4 class="card-title">Songs List</h4>
        <h6 class="card-subtitle"></h6>
        <div class="table-responsive m-t-40">
            <table id="listTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>ACTION</th>
                        <!-- <th>ID</th> -->
                        <th>LISTEN TO SONG</th>
                        <th>THUMBNAIL</th> 
                        <th class="hidden-xs hidden-sm">DESCRIPTION</th>
                        @if(auth()->user()->type == "admin" || auth()->user()->type == "developer")
                        <th class="hidden-xs hidden-sm">STATUS</th>
                        @endif
                       
                    </tr>
                </thead>
                <tbody>
                @php ($i = 0)
                    @foreach($records as $k => $song)
                    <tr>
                        <td style="width:300px">
                            @if(isset($song->audio) && !empty($song->audio)) 
                            @php ($audiopath = url('images/audio/'.$song->audio))   
                            <div class="song-container hidden-xs hidden-sm" id="play_song_{{$song->id}}" >                        
                                <audio controls>
                                    <source src="{{asset($audiopath)}}" type="audio/mpeg"> 
                                </audio>
                            </div>
                            @endif
                            @if(auth()->user()->type == 'admin')
                            <a href="{{route('songs.edit',$song->id)}}" class=" edit fa fa-edit green" data-toggle="tooltip" title="Edit Song">
                            </a>
                            <a href="{{route('songs.delete',$song->id)}}" class=" delete fa fa-trash-o red" data-toggle="tooltip" title="Delete Song"></a>
                            @endif

                            @if(isset($song->appericiation_letter) && !empty($song->appericiation_letter)) 
                            <a href="{{route('songs.pdf_view',$song->id)}}" class="orange" data-toggle="tooltip" title="View Appreciation Letter">
                            <i class="fa fa-file-pdf-o"></i>View Appreciation Letter</a>
                            @endif

                            <a href="{{route('songs.view',$song->id)}}" class="blue" data-toggle="tooltip" title="View Details">
                            <i class="fa fa-eye"></i>View</a>
                        </td>
                        <!-- <td>{{++$i}}</td> -->
                        <td class="link" style="width:200px"><a href="/play/song/{{$song->id}}">{{ ucwords(strtolower($song->title)) }}</a></td>
                        <td class="song-thumbnail" style="width:100px">
                        @php ($thumbnailPath = "images/thumbnail/no-thumbnail.jpg")
                        @if (!empty($song->thumbnail))
                            @php ($thumbnailPath ="images/thumbnail/" . $song->thumbnail)
                            @php ($thumbnail_name = $song->thumbnail)
                        @endif
                        <img src="{{asset($thumbnailPath)}}" >
                        
                        </td>
                        <td class="hidden-xs hidden-sm" style="width:250px">{{$song->desc}}</td>
                        @if(auth()->user()->type == "admin" || auth()->user()->type == "developer")
                        <td class="hidden-xs hidden-sm">
                          @if($song->status == '1')
                            <i class="badge badge-success">Active</i>
                          @elseif($song->status == '0')
                          <i class="badge default">Inactive</i>
                          @endif
                        </td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>