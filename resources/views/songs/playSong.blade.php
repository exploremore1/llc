<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/llc.png')}}">
    <title>Loyalty Reward Song</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('css/library/bootstrap.min.css')}}" rel="stylesheet">
      <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{asset('css/library/bootstrap-datepicker3.min.css')}}" rel="stylesheet">
 
    <link href="{{asset('css/helper.css')}}" rel="stylesheet">
    <link href="{{asset('css/library/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/library/jquery-ui.css')}}" rel="stylesheet">
    
<![endif]-->
</head>

<body class="fix-header fix-sidebar">
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>

<!-- Main wrapper  -->
<div id="main-wrapper" class="play-song-page">
    <!-- Bread crumb -->
    <div class="row page-titles song-head">
        <div class="col-md-12 align-self-center">
            <h3 class="text-primary"><i class="fa fa-music"></i>Loyalty Reward Song</h3> 
        </div>
        
    </div>
    <!-- End Bread crumb -->

<!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start ADD/EDIT Content -->
        <div class="row page-titles song-main-container">
            <div class="card-title blue-bg">
                <h4>{{isset($song->title) ? ucwords(strtolower($song->title)) : ''}}</h4>  
            </div>
            <div class="card-body">
                <div class="basic-form col-md-12">
                    <!-- start of integrations-->
                    <div class="row">
                        <div class= "col-md-12 col-xs-12">
                            <!-- <div class= "pull-right">
                                <span>
                                    !-- Start Fb button  --
                                    <div id="fb-root"></div>
                                        !-- Your share button code --
                                        @php ($hrefPath =   url('/') .'/play/song/'. $song->id )
                                    <div class="fb-share-button" 
                                        data-href= "{{$hrefPath}}"
                                        data-size="large"
                                     > 
                                    </div>
                                </span>
                            </div> -->
                            <!-- End Fb button  -->
                            <!-- Start Twitter button  -->
                            <!-- <div class= "pull-right">
                                <span>
                                    <div>
                                    <a href="https://twitter.com/share?text=Listen to this amazing track on &url= <?=$hrefPath;?>&via=LoyaltyRewards" target="_blank">
                                        <img src="/images/twitter.png" width="25" height="25" title="Share it" /></a>
                                    </div>
                                </span>
                            </div> -->
                            <!-- End Twitter button  -->
                            
                            <!-- Start LinkedIn button  -->
                            <!-- <div class= "pull-right">
                                <span>
                                    <div>
                                        <a href="https://www.linkedin.com/sharing/share-offsite/?url=https://loyaltyrewardssong.com/play/song/4/44 "target="_blank" title="Share on LinkedIn">
                                            <img src="/images/linkedin.png" width="20" height="20" title="Share it" /></a>
                                        </a>
                                    </div>
                                </span>
                            </div> -->
                            <!-- End LinkedIn button  -->
                        </div>
                    </div>
                    <!-- end of integrations-->
                    <!-- row of thumbnail & PDF -->
                    <div class='row'>
                        @if(!empty($filePath))
                            <!--Thumbnail -->
                            <div class= "col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-0">
                                <div class="thumbnail-container thumb-img">
                                    @php ($thumbnailPath = "images/thumbnail/no-thumbnail.jpg")
                                    @if (!empty($song->thumbnail))
                                        @php ($thumbnailPath ="images/thumbnail/" . $song->thumbnail)
                                        @php ($thumbnail_name = $song->thumbnail)
                                    @endif
                                    <img src="{{asset($thumbnailPath)}}"  > 
                                </div>
                            </div>
                            <!--Thumbnail -->

                            <!--Start of PDF -->
                                <div class= "col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-0  thumbnail-container pdf-desktop-view">
                                    <embed src="{{ url($filePath) }}" type="application/pdf" width="100%" style="height:380px;frameborder=0">                                    
                                </div>  
                                <div class= "col-xs-10 col-xs-offset-1 col-md-12 col-md-offset-0 text-center thumbnail-container pdf-mobile-view">
                                    <a href="{{ url($filePath) }}" target="_blank" class="btn btn-primary appreciation-btn">View Appreciation Letter</a>
                                </div> 
                                
                            <!--End of PDF -->
                        @else
                        <div class= "col-md-12">
                            <div class=" d-flex justify-content-center text-center thumbnail-container thumb-img">
                                @php ($thumbnailPath = "images/thumbnail/no-thumbnail.jpg")
                                @if (!empty($song->thumbnail))
                                    @php ($thumbnailPath ="images/thumbnail/" . $song->thumbnail)
                                    @php ($thumbnail_name = $song->thumbnail)
                                @endif
                                <img src="{{asset($thumbnailPath)}}" >
                            </div>
                        </div>
                        @endif
                    </div>

                    @if(!empty($discount))
                    <div class='row'>
                        <!-- Start of Discount -->
                        <div class= "col-md-12 col-xs-12">
                            <div class="col-4 d-flex justify-content-center text-center thumbnail-container ">
                                <a href="{{ $product_link }}">
                                    <button class="btn btn-primary appreciation-btn"> CLICK HERE TO GET {{$discount}}% OFF</button>
                                </a>
                            </div>
                        </div>
                        <!-- End of Discount -->                        
                    </div>
                    @endif
                    <div class='row'>
                        <div class="col-xs-10 col-xs-offset-1 col-md-12 col-md-offset-0 text-center audio-player-container">
                            @if(isset($song->audio) && !empty($song->audio)) 
                            @php ($audiopath = url('images/audio/'.$song->audio))   
                                <div class="song-container">                        
                                    <audio controls autoplay>
                                        <source src="{{asset($audiopath)}}" type="audio/mpeg"> 
                                    </audio>
                                </div>
                            @endif
                        </div> 
                    </div><!-- END OF ROW -->

                </div><!-- END OF basic-form col-md-12  --> 
            </div> <!-- END OF card-body  -->    
        </div>  <!-- END OF song-main-container -->  
        <!-- End ADD/EDIT Content -->
          <!-- footer -->
    <footer class="footer footer-song-page"> © 2021-2022 All rights reserved.</footer>
     <!-- End footer -->
    </div>     
<!-- End Container fluid  -->

  
</div>
<!-- End Wrapper -->
    
    <!-- All Jquery -->
    <script src="{{asset('js/library/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{asset('js/library/popper.min.js')}}"></script>
    <script src="{{asset('js/library/bootstrap.min.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{asset('js/library/jquery-ui.js')}}"></script>
    <script src="{{asset('js/library/chosen.jquery.min.js')}}"></script>
      
    <script src="{{asset('js/library/scripts.js')}}"></script>
    <script src="{{asset('js/library/default.js')}}"></script>
    <script src="{{asset('js/library/toastr.min.js')}}"></script>
    <script src="{{asset('js/library/datatables.min.js')}}"></script>
    
       @yield('page-js-files')
       @yield('page-js-script')
</body>

</html>