@extends('layouts.admin')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles blue-bg">
    <div class="col-md-5 align-self-center">
        <h3 class="text-white"><i class="fa fa-music"></i>Songs</h3> </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Songs</li>
        </ol>
    </div>
</div>
<!-- End Bread crumb -->

<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start Report View -->
    <div class="row page-titles">
        <div class="col-md-12">
            <div class="panel-group panel-collapse-upload">
                <div class="panel panel-default">
                    <div class="panel-heading">View Details
                        <a href="{{ route('songs.list') }}" class="btn btn-primary pull-right view-back-btn"><i class="fa fa-arrow-left"></i> BACK</a>
                    </div>
                    <div class="panel-body view-panel">
                        <div class="row"> 
                            <div class="col-md-3 col-xs-12">Song Title:</div>
                            <div class="col-md-9 col-xs-12">{{ isset($song["title"])? ucwords(strtolower($song->title)):'' }}</div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-3 col-xs-12">Thumbnail</div>
                                @php ($thumbnailPath = "images/thumbnail/no-thumbnail.jpg")
                                 @if (!empty($song->thumbnail))
                                @php ($thumbnailPath ="images/thumbnail/" . $song->thumbnail)
                                @php ($thumbnail_name = $song->thumbnail)
                                @endif
                            <div class="col-md-9 col-xs-12">
                                <img style="width:90px" src="{{asset($thumbnailPath)}}" >
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-3 col-xs-12">Description</div>
                            <div class="col-md-9 col-xs-12"> {{ isset($song["desc"])? $song->desc :'' }}</div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-3 col-xs-12">Status:</div>
                                @if($song->status)
                                    <div class="col-md-9 col-xs-12"> <i class="badge badge-success">Active</i></div>
                                @elseif(!$song->status)
                                    <div class="col-md-9 col-xs-12"> <i class="badge default">Inactive</i></div>
                                @endif
                            </div>
                        </div>
                        @if(isset($song))
                        @php ($audiopath = url('images/audio/'.$song["audio"])) 
                        <div class="row margin-bottom-10"> 
                            <div class="song-container col-md-6 col-xs-12 col-sm-12">  
                                <audio controls>
                                    <source src="{{asset($audiopath)}}" type="audio/mpeg"> 
                                </audio>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <!-- End Report View -->
</div>
<!-- End Container fluid  -->
@endsection