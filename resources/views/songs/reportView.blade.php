@extends('layouts.admin')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles blue-bg">
    <div class="col-md-5 align-self-center">
        <h3 class="text-white"><i class="fa fa-music"></i>Songs</h3> </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Songs</li>
        </ol>
    </div>
</div>
<!-- End Bread crumb -->

<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start Report View -->
    <div class="row page-titles">
        <div class="col-md-12">
            @if(!empty($filePath))
            <div class="panel-group panel-collapse-upload">
                <div class="panel panel-default">
                    <div class="panel-heading">View Appericiation Letter_{{$item["title"]}}
                    <a href="{{ route('songs.list') }}" class="btn btn-primary pull-right view-back-btn"><i class="fa fa-arrow-left"></i> BACK</a>
                    </div>
                    <embed src="{{ url($filePath) }}" type="application/pdf" width="100%" style="height:800px;" frameborder="0">
                </div>
            </div>
            @else
            <div class="alert alert-primary">
                No letter Found for this song
            </div>
            @endif
        </div>
    </div>
    <!-- End Report View -->
</div>
<!-- End Container fluid  -->
@endsection