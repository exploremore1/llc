<div class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#add">
                    <span class="glyphicon glyphicon-plus"></span> 
                    <strong id="titles">{{ (!isset($id))?"ADD NEW SONG":"UPDATE SONG" }}</strong>
                </a>
            </h4>
        </div>
        <div id="add" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="card-body">
                    <div class="basic-form col-md-6">
                        <form method="post" action="{{ (!isset($id))?url('/edit/song/'):url('/edit/song/'.$id)}}"  id="addedit_form" enctype="multipart/form-data">
                        <div class="form-group">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                            <!--<input type="hidden" value="{{csrf_token()}}" name="_token" />-->
                            <label for="tag">Title:<span class="asterisk">*</span></label>
                            <input type="text"  required class="form-control" name="title" value="{{ isset($item['title'])?$item['title']:'' }}"/>
                            </div>
                            <div class="form-group">
                                <label for="tag">Link:<span class="asterisk">*</span></label>
                                <input type="text" required class="form-control" name="link" value="{{ isset($item['link'])?$item['link']:'' }}"/>
                            </div>
                            <div class="form-group">
                                <label for="tag">Description:</label>
                                <input type="text" class="form-control" name="desc" value="{{ isset($item['desc'])?$item['desc']:'' }}"/>
                            </div>
                            <div class="form-group">
                                <label for="tag">Appericiation letter:</label>
                                <input type="file" class="form-control" name="appericiation_letter" value="" />
                            </div>    
                            <div class="form-group">  
                            @if(isset($item['appericiation_letter']) && isset($id))                            
                                    {{ $item['appericiation_letter'] }}
                            @endif
                            </div>
                            <div class="form-group">
                                <label for="tag">Thumbnail:</label>
                                <input type="file" class="form-control" name="thumbnail" value="" />
                            </div>    
                            <div class="form-group">  
                            @if(isset($item['thumbnail']) && isset($id))                            
                                <img src="{{asset('images/thumbnail/'. $item['thumbnail'])}}"  width="25px" height="25px">
                                    {{ $item['thumbnail'] }}
                            @endif
                            </div>
                            <div class="form-group">
                                <label for="tag">Upload Song Audio:<span class="asterisk">*</span></label>
                                <input type="file" required class="form-control" name="audio"/>
                            </div>
                            <div class="form-group">  
                            {{ (isset($item['audio']) && isset($id)) ? $item['audio'] : '' }}
                            </div>
                            <div class="form-group">
                                <label for="tag">Status:<span class="asterisk">*</span></label>
                                <select class="form-control" name="status" required>
                                    <option value=1 {{ ((isset($item["status"]) && $item["status"])?'selected':'') }} >Yes</option>
                                    <option value=0 {{ ((isset($item["status"]) && !$item["status"])?'selected':'') }} >No</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary" id="addedit_form_submit">{{ (!isset($id))? "CREATE" :"UPDATE"}}</button>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

