<div class="card">
    <div class="card-body">
        <h4 class="card-title">Rewards List</h4>
        <h6 class="card-subtitle"></h6>
        <div class="table-responsive m-t-40">
            <table id="listTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>ACTION</th>
                        <!-- <th>ID</th> -->
                        @if (auth()->user()->type != 'customer')
                        <th>CUSTOMER NAME </th>
                        @endif
                        <th class="hidden-xs hidden-sm">THUMBNAIL</th> 
                        <th>LISTEN TO SONG</th>
                        <th class="hidden-xs hidden-sm">PRODUCT DISCOUNT</th>
                    </tr>
                </thead>
                <tbody>
                @php ($i = 0)
                    @foreach($records as $k => $record)
                    <tr>    
                        <td>
                            @if(isset($record->song_audio) && !empty($record->song_audio))   
                            @php ($audiopath = url('images/audio/'.$record->song_audio)) 
                            <div class="song-container hidden-xs hidden-sm" id="play_song_{{$record->id}}" style="display:none;">                        
                                <audio controls>
                                    <source src="{{asset($audiopath)}}" type="audio/mpeg"> 
                                </audio>
                            </div>
                            <a class="blue fa fa-music hidden-xs hidden-sm" href="void:javascript(0);" onclick="playSong({{$record->id}})"></a>
                            @endif
                            @if (in_array(auth()->user()->type, array("admin", "business_owner")))
                            <!-- <a href="{{route('rewards.edit',$record->id)}}" class="green edit fa fa-edit" data-toggle="tooltip" title="Edit Record"></a> -->
                            <a href="{{route('rewards.delete',$record->id)}}" class="red delete fa fa-trash-o" data-toggle="tooltip" title="Delete Record"></a>
                            <a href="{{route('rewards.view',$record->id)}}" class="orange" data-toggle="tooltip" title="View">
                            <i class="fa fa-eye"></i>View</a>
                            @endif
                        </td>
                        <!-- <td>{{++$i}}</td> -->
                        @if (auth()->user()->type != 'customer')
                        <td>{{ucwords(strtolower($record->customer_name)) }}</td>
                        @endif
                        <td class="song-thumbnail hidden-xs hidden-sm" style="width:100px">
                        <?php
                            foreach($songs as $k=>$song)
                            {
                                if($song->id == $record->song_id)
                                {
                                    $thumbnail=$song->thumbnail;
                                    break;
                                }
                            }
                        ?>
                        @php ($thumbnailPath = "images/thumbnail/no-thumbnail.jpg")
                        @if (!empty($thumbnail))
                            @php ($thumbnailPath ="images/thumbnail/" . $thumbnail)
                            @php ($thumbnail_name = $thumbnail)
                        @endif
                        <img src="{{asset($thumbnailPath)}}" >
                        
                        </td>
                        <td class="link" style="width:200px"><a href="/play/song/{{$record->song_id}}">{{ ucwords(strtolower($record->song_title)) }}</a></td>                       
                        <td class="link hidden-xs hidden-sm">
                        <a href="{{$record->product_link}}">Click here to get {{$record->discount}}% discount 
                                 </a>
                        </td>
                        
                        
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>