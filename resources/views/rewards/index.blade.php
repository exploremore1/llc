@extends('layouts.admin')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles blue-bg">
    <div class="col-md-5 align-self-center">
        <h3 class="text-white"><i class="fa fa-music"></i>Rewards Dashboard</h3> </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Rewards</li>
        </ol>
    </div>
</div>
<!-- End Bread crumb -->

<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start ADD/EDIT Content -->
    @if (in_array(auth()->user()->type, array("admin", "business_owner")))
    <div class="row page-titles">
        <div class="col-md-12" id="addedit_form_div">
            @include('rewards.edit')
        </div>
    </div> 
    @endif  
    <!-- End ADD/EDIT Content -->
   
    <!-- Start Filter Content -->
    <div id="uploadReport"></div>
   
    <!-- End Filter Content -->
    <!-- Start Filter Content -->
    <div class="row page-titles">
        <div class="col-md-12">
            @include('rewards.filter')
        </div>
    </div>  
    <!-- End Filter Content -->
    <!-- Start List Content -->
    <div id="list">
        @include('rewards.list')
    </div>
    <!-- End List Content -->
</div>
<!-- End Container fluid  -->
@endsection