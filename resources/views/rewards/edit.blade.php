<div class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#add">
                    <span class="glyphicon glyphicon-plus"></span>
                    <strong id="titles">{{ (!isset($id))?"ADD NEW REWARD":"UPDATE REWARD" }}</strong>
                </a>
            </h4>
        </div>
        <div id="add" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="card-body">
                    <div class="basic-form col-md-6">
                        <form method="post" action="{{ (!isset($id))?url('/edit/reward'):url('/edit/reward/'.$id)}}"  id="addedit_form" enctype="multipart/form-data">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        @php
                        $email_content = '';
                        if(isset($record->email_content)){
                            $email_content = unserialize($record->email_content);
                        }
                        @endphp
                            @if(auth()->user()->type == "admin")
                            <div class="form-group">
                                <label for="tag">From (Business Owner):<span class="asterisk">*</span></label>
                                <select name="from_business_owner" required class="form-control" id="from_business_owner" data-attribute="{{url('/edit/reward')}}">
                                <option value="">Select Business Owner</option>
                                @foreach ($business_owners->all() as $k => $owner)
                                    <option value="{{ $owner->id }}/{{ $owner->email }}"
                                    {{ ((isset($record->business_owner_id) && ($owner->id == $record->business_owner_id))?'selected':'') }}
                                    >
                                    {{ ucwords(strtolower($owner->fname)) }} {{ ucwords(strtolower($owner->lname)) }} ({{$owner->email}})
                                    </option>
                                @endforeach
                                </select>
                            </div>
                            @else
                            <div class="form-group">
                                <label for="tag">From (Business Owner):<span class="asterisk">*</span></label>
                                <select name="from_business_owner" required class="form-control" id="from_business_owner" data-attribute="{{url('/edit/reward')}}">
                                    <option value="{{ isset($business_owners['id'])?($business_owners['id']):'' }}/{{ isset($business_owners['email'])?($business_owners['email']):'' }}" selected >
                                    {{ isset($business_owners['fname']) ? (ucwords(strtolower($business_owners['fname'])).' '. ucwords(strtolower($business_owners['lname']))):'' }} ({{isset($business_owners['email'])?$business_owners['email']:''}})
                                    </option>
                                </select>
                            </div>
                            @endif

                            @if(auth()->user()->type == "admin")
                            <div class="form-group">
                                <label for="to">To:<span class="asterisk">*</span></label>
                                <select name="to_customer" required class="form-control" id="to_customer" >
                                    <option value="">Select Customer</option>
                                    @if(isset($record->customer_id) && !empty($record->customer_id))
                                        <option value="{{ $record->customer_id }}/{{ $record->customer_email }}"  selected>
                                        {{ $record->customer_name }} ({{ $record->customer_email }})
                                        </option>
                                    @endif
                                </select>
                            </div>
                            @else
                            <div class="form-group">
                                <label for="to">To:<span class="asterisk">*</span></label>
                                <select name="to_customer" required class="form-control" id="to_customer" >
                                    <option value="">Select Customer</option>
                                    @foreach ($customers->all() as $k => $customer)
                                        <option value="{{ $customer->id }}/{{ $customer->email }}"
                                        {{ ((isset($record->id) && ($record->customer_id == $customer->id))?'selected':'') }}
                                        >
                                        {{ ucwords(strtolower($customer->fname)) }} {{ ucwords(strtolower($customer->lname)) }} ({{$customer->email}})
                                    </option>
                                @endforeach
                                </select>
                            </div>
                            @endif
                            <div class="form-group">
                                <label for="cc">Cc:</label>
                                <input type="text" class="form-control" placeholder="cc" name="cc" value="{{ isset($email_content['cc']) ? $email_content['cc']:'' }}"/>
                            </div>

                            <div class="form-group">
                                <label for="bcc">Bcc:</label>
                                <input type="text" class="form-control" placeholder="bcc" name="bcc" value="{{ isset($email_content['bcc']) && !empty($email_content['bcc']) ? $email_content['bcc']:'pallavikhokhar2106@gmail.com' }}"/>
                            </div>

                            <div class="form-group">
                                <label for="tag">Songs:<span class="asterisk">*</span></label>
                                <select name="song_id" required class="form-control" id="song_id" >
                                <option value="" >Select Song</option>
                                @foreach ($songs->all() as $k => $song)
                                    <option value="{{ $song->id }}"
                                   {{ isset($record->song_id) && ($song->id == $record->song_id)  ? 'selected' : '' }}
                                   >
                                    {{ ucwords(strtolower($song->title)) }}
                                    </option>
                                @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tag">Product's Link:<span class="asterisk">*</span></label>
                                <input type="text"  name="product_link" required class="form-control"  value="">
                            </div>
                            <div class="form-group">
                                <label for="tag">Discount Percentage:<span class="asterisk">*</span></label>
                                <select name="discount" required class="form-control" id="discount" >
                                <option value="" >Select Discount</option>
                                <option value="10"
                                {{ isset($record->discount) && ($record->discount == '10')  ? 'selected' : '' }}
                                 >10%</option>
                                <option value="20" 
                                {{ isset($record->discount) && ($record->discount == '20')  ? 'selected' : '' }}
                                >20%</option>
                                <option value="30"
                                {{ isset($record->discount) && ($record->discount == '30')  ? 'selected' : '' }}
                                 >30%</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tag">Subject:<span class="asterisk">*</span></label>
                                <input type="text" required name="subject" required class="form-control" placeholder="Rewards" value="{{ isset($email_content['subject']) ? $email_content['subject']:'Here\'s Your Appreciation Reward Song!' }}">
                            </div>
                            <div class="form-group">
                                <label for="tag">Body:<span class="asterisk">*</span></label>
                                <textarea name="body" required rows="6" cols="80" class="form-control" style="height:200px"> @if( isset($email_content['body']) && !empty($email_content['body'])) {{  strip_tags(stripslashes($email_content['body']))  }}  @else 

I just want to thank you for being such an amazing 
customer/subscriber! You have have shown amazing trust
in "Loyalty Reward Song" !!

So I am sending you  a special reward to show you my 
appreciation!

It's my special reward  to show how much I really appreciate 
your trust! I won't mention it here as I don't want to spoil 
the surprise. :)

Hint just visit the following link and click on the left link.

IMPORTANT! I also have another surprise for you on the right
side of the page.

Go here now to get your deserved award and thank you for being
the most important part of our success!

Click on the below mentioned button to get Your Appreciation Reward Now!
Listen to this amazing track and enjoy!!

@endif
</textarea>
                            </div>

                            <button type="submit" class="btn btn-primary" id="addedit_form_submit">{{ (!isset($id))? "CREATE" :"UPDATE"}}</button>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

