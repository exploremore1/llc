@extends('layouts.admin')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles blue-bg">
    <div class="col-md-5 align-self-center">
        <h3 class="text-white"><i class="fa fa-music"></i>Rewards</h3> </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Rewards</li>
        </ol>
    </div>
</div>
<!-- End Bread crumb -->

<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start Report View -->
    <div class="row page-titles">
        <div class="col-md-12">
            <div class="panel-group panel-collapse-upload">
                <div class="panel panel-default">
                    <div class="panel-heading">View Details
                        <a href="{{ route('rewards.list') }}" class="btn btn-primary pull-right view-back-btn"><i class="fa fa-arrow-left"></i> BACK</a>
                    </div>
                    <div class="panel-body view-panel">
                        <div class="row"> 
                            <div class="col-md-3 col-xs-12">Customer Name:</div>
                            <div class="col-md-9 col-xs-12">{{ isset($user->fname) ? ucwords($user->fname) : '' }} {{ isset($user->lname) ? ucwords($user->lname) : '' }}</div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-3 col-xs-12">Song Title:</div>
                            <div class="col-md-9 col-xs-12"> {{ isset($song["title"])? ucwords(strtolower($song->title)):'' }}</div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-3 col-xs-12">Product Discount:</div>
                            <div class="col-md-9 col-xs-12">
                            <a href="{{isset($record->product_link) ?$record->product_link : '-'}}">Click to get {{isset($record->discount) ?$record->discount : ''}}% Discount</a>
                            </div>
                        </div>
                        @if(isset($song))
                        @php ($audiopath = url('images/audio/'.$song["audio"])) 
                        <div class="row margin-bottom-10"> 
                            <div class="song-container col-md-6 col-xs-12 col-sm-12">  
                                <audio controls>
                                    <source src="{{asset($audiopath)}}" type="audio/mpeg"> 
                                </audio>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <!-- End Report View -->
</div>
<!-- End Container fluid  -->
@endsection