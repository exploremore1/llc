<div class="card-title">
    <h4>Filter Rewards List</h4>  
</div>
<div class="card-body">
    <div class="basic-form col-md-12">
        <form method="post" action="{{url('/find/rewards')}}" id="filter_form">
            <div class="form-group">
                <input type="hidden" value="{{csrf_token()}}" name="_token" />
                <div class='row expand_row'>
                    <div class='col-md-4 filterRow'>
                    @if (auth()->user()->type == 'business_owner')
                    @php ($placeholder = 'Customer')
                    @elseif (auth()->user()->type == 'customer')
                    @php ($placeholder = 'Business Owner')
                    @else
                    @php ($placeholder = 'Customer / Business Owner')
                    @endif
                        <input type="text" 
                            class="form-control"
                            name="value" 
                            placeholder="{{$placeholder}}"
                             id="filter_title" />
                    </div>
                    <div class='col-md-4'>
                        <button class="btn btn-primary" id="filter_form_submit">Search</button>&nbsp;
                        <button class="btn btn-default" id="filter_form_reset">Reset</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>