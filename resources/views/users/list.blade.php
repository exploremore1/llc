<div class="card">
    <div class="card-body">
        <h4 class="card-title">User List</h4>
        <h6 class="card-subtitle"></h6>
        <div class="table-responsive m-t-40">
            <table id="listTable" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>Action</th>
                        <!-- <th>ID</th> -->
                        <th>NAME</th>
                        <th class="hidden-xs hidden-sm">EMAIL</th>
                        <th class="hidden-xs hidden-sm">STATUS</th>
                    </tr>
                </thead>
                <tbody>
                @php ($i = 0)
                    @foreach($users as $k => $user)
                    <tr>
                        <td>
                            @if(auth()->user()->type == 'admin')
                            <a href="{{route('edit',['id'=>$user->id, 'type'=>'password'])}}"
                                class="editpassword fa fa-key blue"  data-toggle="tooltip" title="Edit Password">
                                </a>
                            @endif   
                            <a href="{{route('edit',$user->id)}}" class="edit fa fa-edit green" data-toggle="tooltip" title="Edit User">
                            </a>
                            <a href="{{route('delete',$user->id)}}" class="delete fa fa-trash-o red" data-toggle="tooltip" title="Delete User"></a>
                            <a href="{{route('view',$user->id)}}" class="blue" data-toggle="tooltip" title="View Details">
                            <i class="fa fa-eye"></i>View</a>
                        </td>
                        <!-- <td>{{++$i}}</td> -->
                        <td>{{ucwords(strtolower($user->fname))}} {{ucwords(strtolower($user->lname))}}</td>
                        <td class="hidden-xs hidden-sm">{{$user->email}}</td>
                        <td class="hidden-xs hidden-sm">
                          @if($user->active == '1')
                            <i class="badge badge-success">Active</i>
                          @elseif($user->active == '0')
                          <i class="badge default">Inactive</i>
                          @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>