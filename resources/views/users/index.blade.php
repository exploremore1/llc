@extends('layouts.admin')

@section('content')
<!-- Bread crumb -->
<div class="row page-titles blue-bg">
    <div class="col-md-5 align-self-center">
        <h3 class="text-white"><i class="fa fa-users"></i> Users</h3> </div>
    <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Users</li>
        </ol>
    </div>
</div>
<!-- End Bread crumb -->

<!-- Container fluid  -->
<div class="container-fluid">
    <!-- Start ADD/EDIT Content -->
    <div class="row page-titles">
        <div class="col-md-12" id="addedit_form_div">
            @include('users.edit')
        </div>
    </div>  
    <!-- End ADD/EDIT Content -->
    <!-- Start Filter Content -->
    <div class="row page-titles">
        <div class="col-md-12">
            @include('users.filter')
        </div>
    </div>  
    <!-- End Filter Content -->
    <!-- Start List Content -->
    <div id="list">
        @include('users.list')
    </div>
    <!-- End List Content -->
</div>
<!-- End Container fluid  -->
@endsection