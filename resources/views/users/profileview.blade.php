<div class="col-xs-12 col-sm-12 col-md-3">
    <a class="edit_image fa fa-image col-xs-12" href="#" aria-expanded="true">
        <span class="hide-menu">Change</span>
    </a>
    <?php
    $imagePath = "images/no_avatars.jpg";
    if (isset($users->image)) {
        $imagePath = "images/avatars/" . $users->image;
    }
    ?>
    <img src="{{asset($imagePath)}}" alt="user" class="profile-pic" height="150px" />
</div> 
<div class="col-xs-12 col-sm-12 col-md-8 profile-view-container">
    <div class="col-md-3 profile-left-text">Name:</div>
    <div class="col-md-9 profile-right-text"><strong>{{ isset($users->fname) ? ucwords($users->fname) : '' }} {{ isset($users->lname) ? ucwords($users->lname) : '' }}</strong></div>
    <div class="col-md-12"><hr></div>
    <div class="col-md-3 profile-left-text">Email:</div>
    <div class="col-md-9 profile-right-text"><strong>{{ isset($users->email) ? $users->email : '--'}}</strong></div>
    <div class="col-md-12"><hr></div>
    
    <div class="col-md-3 profile-left-text">Gender:</div>
    <div class="col-md-9 profile-right-text"><strong>@if(!empty($users->gender)){{ ucwords($users->gender) }} @else {{'--'}} @endif</strong></div>
    <div class="col-md-12"><hr></div>   
    
    <div class="col-md-3 profile-left-text">Last Activity:</div>
    <div class="col-md-9 profile-right-text"><strong>@if(!empty($users->updated_at)){{ $users->updated_at->format('M d, Y') }}@endif</strong></div>
    <div class="col-md-12"><hr></div>   
</div>
        