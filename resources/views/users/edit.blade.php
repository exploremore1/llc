<div class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#add">
                    <span class="glyphicon glyphicon-plus"></span> 
                    <strong id="title">{{ (!isset($id))?"ADD":" UPDATE" }}</strong>
                </a>
            </h4>
        </div>
        <div id="add" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="card-body">
                    <div class="basic-form col-md-6">
                        <form method="post" action="{{ (!isset($id))?url('/edit/user'):url('/edit/user/'.$id)}}"  id="addedit_form">

                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            
                            <div class="form-group">
                                <label for="name">First Name:<span class="asterisk">*</span></label>
                                <input type="text" required class="form-control" name="fname" value="{{ isset($users['fname'])?$users['fname']:'' }}"/>
                            </div>
                            <div class="form-group">
                                <label for="tag">Last Name:<span class="asterisk">*</span></label>
                                <input type="text" required class="form-control" name="lname" value="{{ isset($users['lname'])?$users['lname']:'' }}"/>
                            </div>
                            <div class="form-group">
                                <label for="gender">Gender:<span class="asterisk">*</span></label>
                                <select class="form-control" name="gender" required>
                                    <option value="" >Select Gender</option>
                                    <option value=male {{ (isset($users["gender"]) && ($users["gender"] == 'male')) ? 'selected':'' }} > Male </option>
                                    <option value=female {{ (isset($users["gender"]) && ($users["gender"] == 'female')) ? 'selected':'' }}> Female </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tag">Email:<span class="asterisk">*</span></label>
                                <input type="text" required class="form-control" name="email" value="{{ isset($users['email'])?$users['email']:'' }}"/>
                            </div>

                            @if(empty($id))
                            <div class="form-group">
                                <label for="tag">Password:<span class="asterisk">*</span></label>
                                <input type="password" required class="form-control" name="password" value="{{ isset($users['password'])?$users['password']:'' }}"/>
                            </div>
                            <div class="form-group">
                                <label for="password-confirm">Confirm Password:<span class="asterisk">*</span></label>
                                <input type="password" required class="form-control" id="password_confirmation" name="password_confirmation" value="{{ isset($users['password'])?$users['password']:'' }}"/>
                            </div>
                            @endif
                            @if(auth()->user()->type=="business_owner")
                            <div class="form-group">
                                <label for="organization">Organization:</label>
                                <input type="text" class="form-control" id="organization" name="organization" value="{{ isset($users['organization'])?$users['organization']:'' }}"/>
                            </div>
                            @elseif(auth()->user()->type=="customer")
                            <div class="form-group">
                                <label for="plan">Plan:</label>
                                <select class="form-control" name="plan">
                                    <option value="" >Select Plan</option>
                                    <option value="basic" {{ ((isset($users['plan']) && $users['plan'] == "basic")?'selected':'') }} >Basic</option>
                                    <option value="premium" {{ ((isset($users['plan']) && $users['plan'] == "premium")?'selected':'') }} >Premium</option>
                                </select>
                            </div>
                            @endif
                            <div class="form-group">
                                <label for="contact">Contact:</label>
                                <input type="text" class="form-control" id="contact" name="contact" value="{{ isset($users['contact'])?$users['contact']:'' }}"/>
                            </div>
                            <!-- <div class="form-group">
                                <label for="tag"> User Type:</label>
                                <select class="form-control" name="type" required>
                                    <option value="" disabled >Select Type</option>
                                    <option value="admin" {{ ((isset($users["type"]) && $users["type"] == "owner")?'selected':'') }} >Admin</option>
                                    <option value="business_owner" {{ ((isset($users["type"]) && $users["type"] == "business_owner")?'selected':'') }} >Business Owner</option>
                                    <option value="customer" {{ ((isset($users["type"]) && $users["type"] == "customer")?'selected':'') }} >Customer</option>
                                </select>
                            </div> -->

                            <div class="form-group">
                                <label for="tag">Active:<span class="asterisk">*</span></label>
                                <select class="form-control" name="active" required>
                                    <option value="" >Select Status</option>
                                    <option value=1 {{ ((isset($users["active"]) && $users["active"] == 1)?'selected':'') }} >Yes</option>
                                    <option value=0 {{ ((isset($users["active"]) && $users["active"] == 0)?'selected':'') }} >No</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary" id="addedit_form_submit">{{ (!isset($id))? "CREATE" :"UPDATE"}}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

