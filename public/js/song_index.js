//Js is a shorthand notation of javascript
$().ready(function(){
    
    //Toggle PlusMinus
    togglePlusMinus();

    //bind datatable
    toggleDataTables("#list table");
     
    $("#addedit_form_submit").click(addActions);
    $("#addedit_form").submit(addActions);
   
    $("#filter_form_submit").click(submitForm);
    $("#filter_form").submit(submitForm);
    $("#filter_form_reset").click(function(event){
        $("#list").fadeOut("slow",function(){
        $("#list").html();

        $("#filter_title").val("");
        
        $('html, body').animate({
            scrollTop: $(".row").offset().top
        }, 1000);

        loadList();
    });
    });
    
    $(".upload_image").hide();  
    eventActions();

});

function submitForm(event)
{
    stopEvent(event);
    loadList();
}

function eventActions()
{
    $('#listTable').on('click', '.delete', function() {
        deleteActions(event);
    });
    $('#listTable').on('click', '.edit', function() {
        editActions(event);
    });

    //Initialize select2
    $('.select2').select2();
    $('.panel-collapse').collapse('hide');
    
}

function loadList(event)
{
    showOverlay(1);
    
    var postData = $($("#filter_form")).serializeArray();
    var formURL = $($("#filter_form")).attr("action");
    $.ajax(
    {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success" && data['result'])
            {
                $($("#addedit_form input,#addedit_form select,#addedit_form textarea")).each(function(i,node){  $(node).val(""); });
                $('.panel-collapse').collapse('hide');
                $("#list").html(data.data);
                $("#list").fadeIn("slow",function(){
                    $('html, body').animate({
                        scrollTop: $("#list").offset().top
                    }, 1000);
                    $('[data-toggle="tooltip"]').tooltip();
                    eventActions();
                });
                
                toggleDataTables("#list table");
                
            }else{
                toastr.error(data.data,'Message',{
                    timeOut: 5000,
                    "closeButton": true,
                    "tapToDismiss": false
                });
                $('html, body').animate({
                    scrollTop: $("#list").offset().top
                }, 1000);
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
        }
   });
}
var editForm = false;
function addActions()
{
    stopEvent(event);
  console.log(editForm);
    var error = false;
    $(".has-error").removeClass("has-error");
     // Allowing Img type  jpeg,jpg,png,gif|max:2048
     var allowedImgExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
      // Allowing Audio type audio/mpeg,mpga,mp3,wav,aac
     var allowedAudioExtensions = /(\.mp3|\.wav|\.aac|\.mpga)$/i;
     
    $("#addedit_form input,textarea").each(function(i,node){
        if($(node).attr("required")){
            if(node.value == ""){
                if(node.name == 'audio' && editForm == true){
                    error = false;
                }else{
                    error = true;
                    $(node).parent().addClass("has-error");
                }
                
            }else{
                switch(node.name)  {
                    case "audio":
                        if(node.value != ""){
                            if (!allowedAudioExtensions.exec(node.value)) {
                                error = true;
                                $(node).parent().addClass("has-error");
                                toastr.error('Invalid Audio type. Allowed extensions are: audio/mpeg,mpga,mp3,wav,aac','Message',{
                                    timeOut: 5000,
                                    "closeButton": true,
                                    "tapToDismiss": false
                                });
                            } 
                        }
                    break;
                }   
            }
        }else{
            if(node.value != "") {
                switch(node.name) {
                    case "thumbnail":
                        if (!allowedImgExtensions.exec(node.value)) {
                            error = true;
                            $(node).parent().addClass("has-error");
                            toastr.error('Invalid Image type. Allowed extensions are: jpeg,jpg,png,gif','Message',{
                                timeOut: 5000,
                                "closeButton": true,
                                "tapToDismiss": false
                            });
                        }
                    break;
                }   
            }
        }
    });
    
    if(!error)
    {
        showOverlay(1);

        //var postData = $($("#addedit_form")).serializeArray();
        var formURL  = $($("#addedit_form")).attr("action");

        // Get form
        var form = $('#addedit_form')[0];

        // Create an FormData object 
        var formData = new FormData(form);

        $.ajax(
        {
            url : formURL,
            type: "POST",
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            data : formData,
            processData: false,
            contentType: false,
            cache: false,
            success:function(data, textStatus, jqXHR) 
            {
                if(textStatus == "success")
                {
                    if(data.result)
                    {
                        if(data.view)
                        {
                           $("#addedit_form_div").html(data.view);
                           $("#addedit_form_submit").click(addActions);
                           $("#addedit_form").submit(addActions); 
                        }
                        togglePlusMinus();
                        $("#titles").html("ADD NEW SONG");
                        $("#addedit_form_submit").html("CREATE");
                        toastr.success(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                        loadList();
                    }else{
                        hideOverlay(1);
                        toastr.error(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    }
                    
                }
                hideOverlay(1);
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                if(jqXHR.responseJSON.errors){
                    $.each( jqXHR.responseJSON.errors, function( key, value) {
                        toastr.error(value,'Error',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    });
                    
                }else if(jqXHR.responseJSON.exception){
                    toastr.error(jqXHR.responseJSON.message,'Error',{
                        timeOut: 5000,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                }
                hideOverlay(1);
            }
       });
    }else{
        toastr.error('Please fill Required Data','Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
        $('html, body').animate({
            scrollTop: $(".page-titles").offset().top
        }, 1000);
    }
}

function editActions(event)
{
    stopEvent(event);
    showOverlay(1);
    
    var formURL  = $($(event.target)).attr("href");
    $.ajax(
    {
        url : formURL,
        type: "GET",
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
                $("#addedit_form_div").html(data.data);
                //Initialize select2
                $('.select2').select2();
                
                 $('html, body').animate({
                        scrollTop: $(".page-titles").offset().top
                    }, 1000);

                togglePlusMinus();
                $('.panel-collapse').collapse('show');
                editForm = true;
                $("#addedit_form_submit").click(addActions);
                $("#addedit_form").submit(addActions);
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            if(jqXHR.responseJSON.errors){
                $.each( jqXHR.responseJSON.errors, function( key, value) {
                    toastr.error(value,'Error',{
                        timeOut: 5000,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                });
                
            }else if(jqXHR.responseJSON.exception){
                toastr.error(jqXHR.responseJSON.message,'Error',{
                    timeOut: 5000,
                    "closeButton": true,
                    "tapToDismiss": false
                });
            }
            hideOverlay(1);
        }
   });
}

function deleteActions()
{
    if(!confirm("Are you sure to delete this?")){
      event.preventDefault();
        return true;
    }
    
    stopEvent(event);
    showOverlay(1);
    
    var formURL  = $($(event.target)).attr("href");
    $.ajax(
    {
        url : formURL,
        type: "GET",
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
                toastr.success('Record has been deleted successfully','Message',{
                    timeOut: 5000,
                    "closeButton": true,
                    "tapToDismiss": false
                });
                loadList();
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
        }
   });
}

function playSong(id){
    $("#play_song_"+id).slideToggle('1000');
}