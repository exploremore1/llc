//Js is a shorthand notation of javascript
$().ready(function(){
    
    //Toggle PlusMinus
    togglePlusMinus();

    //bind datatable
    toggleDataTables("#list table");
     
    $("#addedit_form_submit").click(addActions);
    $("#addedit_form").submit(addActions);
   
    $("#filter_form_submit").click(submitForm);
    $("#filter_form").submit(submitForm);
    $("#filter_form_reset").click(function(event){
        $("#list").fadeOut("slow",function(){
        $("#list").html();

        $("#filter_title").val("");
        $("#filter_type").val("%");
        $("#filter_type").trigger("chosen:updated");

        $('html, body').animate({
            scrollTop: $(".row").offset().top
        }, 1000);

        loadList();
    });
    });
    
    $(".upload_image").hide();  
    eventActions();
});

function submitForm(event)
{
    stopEvent(event);
    loadList();
}

function eventActions()
{
    $('#listTable').on('click', '.delete', function() {
        deleteActions(event);
    });
    $('#listTable').on('click', '.edit', function() {
        editActions(event);
    });
    $('#listTable').on('click', '.editpassword', function() {
        editActions(event);
    });
    
}

function loadList(event)
{
    showOverlay(1);
    
    var postData = $($("#filter_form")).serializeArray();
    var formURL = $($("#filter_form")).attr("action");
    $.ajax(
    {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
                if(textStatus == "success" && data['result'])
                {
                    $($("#addedit_form input,#addedit_form select,#addedit_form textarea")).each(function(i,node){  $(node).val(""); });
                    $('.panel-collapse').collapse('hide');
                    $("#list").html(data.data);
                    $("#list").fadeIn("slow",function(){
                        $('html, body').animate({
                            scrollTop: $("#list").offset().top
                        }, 1000);
                        $('[data-toggle="tooltip"]').tooltip();
                        eventActions();
                    });
                    
                    toggleDataTables("#list table");
                    
                }else{
                    toastr.error(data.data,'Message',{
                        timeOut: 5000,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                    $('html, body').animate({
                        scrollTop: $("#list").offset().top
                    }, 1000);
                }
                hideOverlay(1);
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
        }
   });
}

function addActions()
{
    stopEvent(event);
    var error = passwordError = false;
    var emailerror = false;
    var emailpattern = /^(([a-zA-Z0-9_.-]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z0-9]{2,4}))+([,;:](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z0-9]{2,4}))+)*$/;
    var passwordpattern = /([a-zA-Z0-9*#@!]{6,20})/;                    
    $(".has-error").removeClass("has-error");
    $("#addedit_form input,#addedit_form textarea, #addedit_form select").each(function(i,node){
        if($(node).attr("required"))
        {
            if(node.value == "")
            {
                error = true;
                $(node).parent().addClass("has-error");

            }
            else
            {
                switch(node.name)
                {
                    case "email":
                        if(node.value != "")
                        {
                            if(!emailpattern.test(node.value))
                            {
                                error = true;
                                emailerror = true;
                                $(node).parent().addClass("has-error");
                            }
                        }
                    break;
                    case "old_password":
                        if (node.value == $("#password").val())
                        {
                            error = true;
                            passwordError = true;
                            $("#old_password").parent().addClass("has-error");
                            $("#password").parent().addClass("has-error");
                            toastr.error('Password/Old password should not be same','Message',{
                               timeOut: 5000,
                               "closeButton": true,
                               "tapToDismiss": false
                           });
                        }
                    break;
                    case "password":
                    case "password_confirmation":
                        if (node.value != $("#password_confirmation").val())
                        {
                           $(node).parent().addClass("has-error");
                           $("#password_confirmation").parent().addClass("has-error");
                           passwordError = true;
                           error = true;
                           toastr.error('Password/confirm password should be same','Message',{
                               timeOut: 5000,
                               "closeButton": true,
                               "tapToDismiss": false
                           });
                        }
                        else if(!passwordpattern.test(node.value))
                        {
                           $(node).parent().addClass("has-error");
                           $("#password_confirmation").parent().addClass("has-error");
                           passwordError = true;
                           error = true;
                           toastr.error('Password should contain atleast 6 characters.Allowed Special Characters are *#@! only!','Message',{
                               timeOut: 5000,
                               "closeButton": true,
                               "tapToDismiss": false
                           });
                        }
                        break;
                        
                }   
            }

        }
    });
    
    if(emailerror){
        toastr.error('Invalid Email','Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
    }
    if(!error)
    {
        showOverlay(1);

        var postData = $($("#addedit_form")).serializeArray();
        var formURL  = $($("#addedit_form")).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            data : postData,
            success:function(data, textStatus, jqXHR) 
            {
                if(textStatus == "success")
                {
                    if(data.result)
                    {
                        if(data.view)
                        {
                           $("#addedit_form_div").html(data.view);
                           $("#addedit_form_submit").click(addActions);
                           $("#addedit_form").submit(addActions); 
                        }
                        $("#title").html("ADD");
                        $("#addedit_form_submit").html("CREATE");
                        togglePlusMinus();
                        toastr.success(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                        loadList();
                        if(data.action == 'change_password'){
                            location.reload();
                        }
                    }else{
                        hideOverlay(1);
                        toastr.error(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    }
                    
                }
                hideOverlay(1);
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                if(jqXHR.responseJSON.errors.email)
                {
                    toastr.error(jqXHR.responseJSON.errors.email,'Message',{
                        timeOut: 0,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                }
                hideOverlay(1);
            }
       });
    }else{
        toastr.error('Please fill Required Data','Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
        $('html, body').animate({
            scrollTop: $(".page-titles").offset().top
        }, 1000);
    }
}

function editActions(event)
{
    stopEvent(event);
    showOverlay(1);
    
    var formURL  = $($(event.target)).attr("href");
    $.ajax(
    {
        url : formURL,
        type: "GET",
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
                $("#addedit_form_div").html(data.data);
                $('html, body').animate({
                    scrollTop: $(".page-titles").offset().top
                }, 1000);
                togglePlusMinus();
                $('.panel-collapse').collapse('show');
                $("#addedit_form_submit").click(addActions);
                $("#addedit_form").submit(addActions);
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            if(jqXHR.responseJSON.errors.email)
            {
                toastr.error(jqXHR.responseJSON.errors.email,'Message',{
                    timeOut: 0,
                    "closeButton": true,
                    "tapToDismiss": false
                });
            }
            hideOverlay(1);
        }
   });
}

function deleteActions()
{
    if(!confirm("Are you sure to delete this?")){
      event.preventDefault();
        return true;
    }
    
    stopEvent(event);
    showOverlay(1);
    
    var formURL  = $($(event.target)).attr("href");
    $.ajax(
    {
        url : formURL,
        type: "GET",
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
                toastr.success('Record has been deleted successfully','Message',{
                    timeOut: 5000,
                    "closeButton": true,
                    "tapToDismiss": false
                });
                loadList();
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
        }
   });
}