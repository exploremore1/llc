//Js is a shorthand notation of javascript
$().ready(function(){
    
    //Toggle PlusMinus
    togglePlusMinus();

    //bind datatable
    toggleDataTables("#list table");
     
    $("#addedit_form_submit").click(addActions);
    $("#addedit_form").submit(addActions);
   
    $("#filter_form_submit").click(submitForm);
    $("#filter_form").submit(submitForm);
    $("#filter_form_reset").click(function(event){
        $("#list").fadeOut("slow",function(){
        $("#list").html();

        $("#filter_title").val("");
        $("#filter_type").val("%");
        $("#filter_type").trigger("chosen:updated");

        $('html, body').animate({
            scrollTop: $(".row").offset().top
        }, 1000);

        loadList();
    });
    });
    
    $(".upload_image").hide();  
    eventActions();
});

function submitForm(event)
{
    stopEvent(event);
    loadList();
}

function eventActions()
{
    $('#listTable').on('click', '.delete', function() {
        deleteActions(event);
    });
    $('#listTable').on('click', '.edit', function() {
        editActions(event);
    });
    $('#listTable').on('click', '.editpassword', function() {
        editActions(event);
    });
    $('#listTable').on('click', '.sendEmail', function() {
        sendEmailTemplateActions(event);
    });
    $('#listTable').on('click', '.row-check', function() {
        $(this).parent().parent().toggleClass('selected');
    });
    $('#send_bulk_email_btn').on('click', function(e){
        sendBulkEmailTemplateActions(event)
    });
    checkallrows();
    
    //To Import customers
    $(".bulkImport").click(importActions);

}

function checkallrows(){
    $("#check_all").on("click", function () {
		if ($("input:checkbox").prop("checked")) {
			$("input:checkbox[name='row-check[]']").prop("checked", true);
		} else {
			$("input:checkbox[name='row-check[]']").prop("checked", false);
		}
    });
}


function loadList(event)
{
    showOverlay(1);
    
    var postData = $($("#filter_form")).serializeArray();
    var formURL = $($("#filter_form")).attr("action");
    $.ajax(
    {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
                if(textStatus == "success" && data['result'])
                {
                    $($("#addedit_form input,#addedit_form select,#addedit_form textarea")).each(function(i,node){  $(node).val(""); });
                    $('.panel-collapse').collapse('hide');
                    $("#list").html(data.data);
                    $("#list").fadeIn("slow",function(){
                        $('html, body').animate({
                            scrollTop: $("#list").offset().top
                        }, 1000);
                        $('[data-toggle="tooltip"]').tooltip();
                        eventActions();
                    });
                    
                    toggleDataTables("#list table");
                    
                }else{
                    toastr.error(data.data,'Message',{
                        timeOut: 5000,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                    $('html, body').animate({
                        scrollTop: $("#list").offset().top
                    }, 1000);
                }
                hideOverlay(1);
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
        }
   });
}

function addActions()
{
    stopEvent(event);
    var error = passwordError = false;
    var emailerror = false;
    var emailpattern = /^(([a-zA-Z0-9_.-]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z0-9]{2,4}))+([,;:](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z0-9]{2,4}))+)*$/;
    var passwordpattern = /([a-zA-Z0-9*#@!]{6,20})/;                    
    $(".has-error").removeClass("has-error");
    $("#addedit_form input,textarea,select").each(function(i,node){
        if($(node).attr("required"))
        {
            if(node.value == "")
            {
                error = true;
                $(node).parent().addClass("has-error");

            }
            else
            {
                switch(node.name)
                {
                    case "email":
                        if(node.value != "")
                        {
                            if(!emailpattern.test(node.value))
                            {
                                error = true;
                                emailerror = true;
                                $(node).parent().addClass("has-error");
                            }
                        }
                    break;
                    
                    case "password":
                    case "password_confirmation":
                        if (node.value != $("#password_confirmation").val())
                        {
                           $(node).parent().addClass("has-error");
                           $("#password_confirmation").parent().addClass("has-error");
                           passwordError = true;
                           error = true;
                           toastr.error('Password/confirm password should be same','Message',{
                               timeOut: 5000,
                               "closeButton": true,
                               "tapToDismiss": false
                           });
                        }
                        else if(!passwordpattern.test(node.value))
                        {
                           $(node).parent().addClass("has-error");
                           $("#password_confirmation").parent().addClass("has-error");
                           passwordError = true;
                           error = true;
                           toastr.error('Password should contain atleast 6 characters.Allowed Special Characters are *#@! only!','Message',{
                               timeOut: 5000,
                               "closeButton": true,
                               "tapToDismiss": false
                           });
                        }
                        break;
                        
                }   
            }

        }
    });
    
    if(emailerror){
        toastr.error('Invalid Email','Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
    }
    if(!error)
    {
        showOverlay(1);

        var postData = $($("#addedit_form")).serializeArray();
        var formURL  = $($("#addedit_form")).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            data : postData,
            success:function(data, textStatus, jqXHR) 
            {
                if(textStatus == "success")
                {
                    if(data.result)
                    {
                        if(data.view)
                        {
                           $("#addedit_form_div").html(data.view);
                           $("#addedit_form_submit").click(addActions);
                           $("#addedit_form").submit(addActions); 
                        }
                        $("#title").html("ADD");
                        $("#addedit_form_submit").html("CREATE");
                        togglePlusMinus();
                        toastr.success(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                        loadList();
                        if(data.action == 'change_password' && data.user_type != 'admin'){
                            location.reload();
                        }
                    }else{
                        hideOverlay(1);
                        toastr.error(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    }
                    
                }
                hideOverlay(1);
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                if(jqXHR.responseJSON.errors.email)
                {
                    toastr.error(jqXHR.responseJSON.errors.email,'Message',{
                        timeOut: 0,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                }
                hideOverlay(1);
            }
       });
    }else{
        toastr.error('Please fill Required Data','Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
        $('html, body').animate({
            scrollTop: $(".page-titles").offset().top
        }, 1000);
    }
}

function editActions(event)
{
    stopEvent(event);
    showOverlay(1);
    
    var formURL  = $($(event.target)).attr("href");
    $.ajax(
    {
        url : formURL,
        type: "GET",
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
                $("#addedit_form_div").html(data.data);
                $('html, body').animate({
                    scrollTop: $(".page-titles").offset().top
                }, 1000);
                togglePlusMinus();
                $('.panel-collapse').collapse('show');
                $("#addedit_form_submit").click(addActions);
                $("#addedit_form").submit(addActions);
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            if(jqXHR.responseJSON.errors.email)
            {
                toastr.error(jqXHR.responseJSON.errors.email,'Message',{
                    timeOut: 0,
                    "closeButton": true,
                    "tapToDismiss": false
                });
            }
            hideOverlay(1);
        }
   });
}

function deleteActions()
{
    if(!confirm("Are you sure to delete this?")){
      event.preventDefault();
        return true;
    }
    
    stopEvent(event);
    showOverlay(1);
    
    var formURL  = $($(event.target)).attr("href");
    $.ajax(
    {
        url : formURL,
        type: "GET",
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
                toastr.success('Record has been deleted successfully','Message',{
                    timeOut: 5000,
                    "closeButton": true,
                    "tapToDismiss": false
                });
                loadList();
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
        }
   });
}

function sendEmailTemplateActions(event)
{
    stopEvent(event);
    showOverlay(1);
    
    var formURL  = $($(event.target)).attr("href");
    $.ajax(
    {
        url : formURL,
        type: "GET",
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
               
                $("#prepareRewardEmail").html("");
                $("#prepareRewardEmail").append(data.data);
                $('html, body').animate({
                    scrollTop: $(".page-titles").offset().top
                }, 1000);
                    //Toggle PlusMinus
                togglePlusMinus();
                $("#sendEmail").click(sendEmailActions);
                $("#sendEmail_form").submit(sendEmailActions);
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            if(jqXHR.responseJSON.errors){
                $.each( jqXHR.responseJSON.errors, function( key, value) {
                    toastr.error(value,'Error',{
                        timeOut: 5000,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                });
                
            }else if(jqXHR.responseJSON.exception){
                toastr.error(jqXHR.responseJSON.message,'Error',{
                    timeOut: 5000,
                    "closeButton": true,
                    "tapToDismiss": false
                });
            }
            hideOverlay(1);
        }
   });
}

function sendEmailActions()
{
    stopEvent(event);
    var error = passwordError = false;
    var emailpattern = /^(([a-zA-Z0-9_.-]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z0-9]{2,4}))+([,;:](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z0-9]{2,4}))+)*$/;
    var passwordpattern = /([a-zA-Z0-9*#@!]{6,20})/;                    
    $(".has-error").removeClass("has-error");
    $("#sendEmail_form input,textarea").each(function(i,node){
        if($(node).attr("required"))
        {
            if(node.value == "")
            {
                error = true;
                $(node).parent().addClass("has-error");

            }{
                switch(node.name)
                {
                    case "to":
                        if(node.value != "")
                        {
                            if(!emailpattern.test(node.value))
                            {
                                error = true;
                                $(node).parent().addClass("has-error");
                                toastr.error('Invalid Email','Message',{
                                    timeOut: 5000,
                                    "closeButton": true,
                                    "tapToDismiss": false
                                });
                            }
                        }
                    break;
                }   
            }
        }
    });
    
    if(!error)
    {
        showOverlay(1);
       
        var postData = $($("#sendEmail_form")).serializeArray();
        var formURL  = $($("#sendEmail_form")).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            data : postData,
            success:function(data, textStatus, jqXHR) 
            {
                if(textStatus == "success")
                {
                    if(data.result)
                    { 
                        $("#prepareRewardEmail").html("");
                        toastr.success(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    }else{
                        hideOverlay(1);
                        toastr.error(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    }
                    
                }
                hideOverlay(1);
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                if(jqXHR.responseJSON.errors){
                    $.each( jqXHR.responseJSON.errors, function( key, value) {
                        toastr.error(value,'Error',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    });
                    
                }else if(jqXHR.responseJSON.exception){
                    toastr.error(jqXHR.responseJSON.message,'Error',{
                        timeOut: 5000,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                }
                hideOverlay(1);
            }
       });
    }else{
        toastr.error('Please fill Required Data','Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
        $('html, body').animate({
            scrollTop: $(".page-titles").offset().top
        }, 1000);
    }
}

function sendBulkEmailTemplateActions(event)
{
    stopEvent(event);
    var selectedRows = [];
    $.each($("input[name='row-check[]']:checked"), function(){
        selectedRows.push($(this).val());
    });

    var error = passwordError = false;
    if(!selectedRows.length)
    {
        error = true;
    }

    if(!error)
    {
        showOverlay(1);
        var postData = { customer_ids: selectedRows }
        var formURL  = $($(event.target)).attr("href")+'/viewBulkEmailTemplate';
        
        $.ajax(
        {
            url : formURL,
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data : postData,
            success:function(data, textStatus, jqXHR) 
            {
                if(textStatus == "success")
                {
                
                    $("#prepareRewardEmail").html("");
                    $("#prepareRewardEmail").append(data.data);
                    $('html, body').animate({
                        scrollTop: $(".page-titles").offset().top
                    }, 1000);
                        //Toggle PlusMinus
                    togglePlusMinus();
                    $("#sendBulkEmail").click(sendBulkEmailActions);
                    $("#sendBulkEmail_form").submit(sendBulkEmailActions);
                }
                hideOverlay(1);
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                if(jqXHR.responseJSON.errors){
                    $.each( jqXHR.responseJSON.errors, function( key, value) {
                        toastr.error(value,'Error',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    });
                    
                }else if(jqXHR.responseJSON.exception){
                    toastr.error(jqXHR.responseJSON.message,'Error',{
                        timeOut: 5000,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                }
                hideOverlay(1);
            }
        });
    }else{
        toastr.warning("Please select atleast 1 Customer",'Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
        $('html, body').animate({
            scrollTop: $(".page-titles").offset().top
        }, 1000);
    }
}

function sendBulkEmailActions(event)
{
    stopEvent(event);
    var error = productLinkError = false;
    $(".has-error").removeClass("has-error");
    $("#sendBulkEmail_form input, #sendBulkEmail_form textarea, #sendBulkEmail_form select").each(function(i,node){
        if($(node).attr("required")){
            if(node.value == ""){
                error = true;
                $(node).parent().addClass("has-error");
            }
            else
            {
                switch(node.name)
                {
                    case "product_link":
                        if(node.value != "")
                        {
                            if(!isValidURL(node.value))
                            {
                                error = true;
                                productLinkError = true;
                                $(node).parent().addClass("has-error");
                            }
                        }
                    break;
                }
            }                    
        }
    });

    if(productLinkError){
        toastr.error('Invalid Product Link','Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
    }
    
    if(!error)
    {
        showOverlay(1);
       
        var postData = $($("#sendBulkEmail_form")).serializeArray();
        var formURL  = $($("#sendBulkEmail_form")).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            data : postData,
            beforeSend: function() {
                toastr.success("It may take some time to send group Email notifications. Please be patient...",'Message',{
                    timeOut: 0,
                    "closeButton": true,
                    "tapToDismiss": false
                });
             },
            success:function(data, textStatus, jqXHR) 
            {
                if(textStatus == "success")
                {
                    if(data.result)
                    { 
                        $("#prepareRewardEmail").html("");
                        toastr.success(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                        $("#check_all").prop("checked", false);
                        $("input:checkbox[name='row-check[]']").prop("checked", false);
                    }else{
                        hideOverlay(1);
                        toastr.error(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    }
                    
                }
                hideOverlay(1);
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                if(jqXHR.responseJSON.errors){
                    $.each( jqXHR.responseJSON.errors, function( key, value) {
                        toastr.error(value,'Error',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    });
                    
                }else if(jqXHR.responseJSON.exception){
                    toastr.error(jqXHR.responseJSON.message,'Error',{
                        timeOut: 5000,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                }
                hideOverlay(1);
            }
       });
    }else{
        toastr.error('Please fill Required Data','Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
        $('html, body').animate({
            scrollTop: $(".page-titles").offset().top
        }, 1000);
    }
}

function isValidURL(string) {
    var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    return (res !== null)
}

function importActions(event)
{
    stopEvent(event);
    showOverlay(1);
    
    var formURL = $($(event.target)).attr("href");
    $.ajax(
    {
        url : formURL,
        type: "GET",
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
                $("#importDiv").html("");
                $("#importDiv").append(data.data);
                $('html, body').animate({
                        scrollTop: $(".page-titles").offset().top
                    }, 1000);
                $('.panel-collapse-upload').collapse('show');

                Dropzone.autoDiscover = false;
                let singleDropzoneOptions = {
                    paramName: "file", // The name that will be used to transfer the file
                    maxFilesize: 2, // MB
                    dictDefaultMessage: "UPLOAD FILE",
                    maxFiles: 1,
                    clickable: true,
                    thumbnailWidth: 140,
                    thumbnailHeight: 140,
                    maxThumbnailFilesize: 0.5,
                    acceptedFiles:".xls,.xlsx,.csv, text/csv, application/vnd.ms-excel, application/csv, text/x-csv, application/x-csv, text/comma-separated-values, text/x-comma-separated-values",
                    init: function () {
                        var self = this;
                        self.options.addRemoveLinks = true;
                        self.options.dictRemoveFile = "Remove";
                        self.on("success", function (file, response) {
                            if(response.result){
                                toastr.success(response.message,'Message',{
                                    timeOut: 2000,
                                    "closeButton": true,
                                    "tapToDismiss": false
                                });
                                loadList();
                            }else{
                                toastr.error(response.message,'Message',{
                                    timeOut: 2000,
                                    "closeButton": true,
                                    "tapToDismiss": false
                                });
                            }
                            $("#importDiv").html("");
                        });
                        self.on("error", function (file, response) {
                            $("#importDiv").html("");
                            if(response.errors)
                            {
                                toastr.error(response.errors.email,'The Data provided in the excel is not in correct form.Some required fields are missing',{
                                    timeOut: 2000,
                                    "closeButton": true,
                                    "tapToDismiss": false
                                });
                            }
                            else{
                                toastr.error(response.message,'The selected file is not an Excel.Please try again',{
                                    timeOut: 2000,
                                    "closeButton": true,
                                    "tapToDismiss": false
                                });
                            }
                           
                        });
                    }
                }
                $("form#import-dropzone").dropzone(singleDropzoneOptions);
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
        }
   });
}



/*
function sendBulkEmailActions(event)
{
    stopEvent(event);
    var selectedRows = [];
    $.each($("input[name='row-check[]']:checked"), function(){
        selectedRows.push($(this).val());
    });

    var error = passwordError = false;
    if(!selectedRows.length)
    {
        error = true;
    }

    if(!error)
    {
        showOverlay(1);
        var postData = { customer_ids: selectedRows }
        var formURL  = $($(event.target)).attr("href");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            data : postData,
            beforeSend: function() {
                toastr.success("It may take some time to send group Email notifications. Please be patient...",'Message',{
                    timeOut: 0,
                    "closeButton": true,
                    "tapToDismiss": false
                });
             },
            success:function(data, textStatus, jqXHR) 
            {
                if(textStatus == "success")
                {
                    if(data.result)
                    { 
                        if(data.view)
                        {
                           $("#addedit_form_div").html(data.view);
                           $("#addedit_form_submit").click(addActions);
                           $("#addedit_form").submit(addActions); 
                        }
                        $("#title").html("CREATE CUSTOMER");
                        $("#addedit_form_submit").html("CREATE");
                        toastr.success(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                        loadList();
                    }else{
                        hideOverlay(1);
                        toastr.error(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    }
                    
                }
                hideOverlay(1);
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                if(jqXHR.responseJSON.errors){
                    $.each( jqXHR.responseJSON.errors, function( key, value) {
                        toastr.error(value,'Error',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    });
                    
                }else if(jqXHR.responseJSON.exception){
                    toastr.error(jqXHR.responseJSON.message,'Error',{
                        timeOut: 5000,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                }
                hideOverlay(1);
            }
       });
    }else{
        toastr.warning("Please select atleast 1 Customer",'Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
        $('html, body').animate({
            scrollTop: $(".page-titles").offset().top
        }, 1000);
    }
}
*/
