//Js is a shorthand notation of javascript
$().ready(function(){
    
    eventActions();
});

function eventActions()
{
    //Bind Edit profile
    $("#addedit_form_submit").click(addActions);
    $("#addedit_form").submit(addActions);
    
    $("#addedit_form_div").hide();  
    $("#uploadAvatars_form_div").hide();  
    
    $('.edit_image').click(uploadAvatars);
    $('.edit_profile').click(editActions);
    $('.edit_password').click(editActions);
}

function uploadAvatars()
{
    stopEvent(event);
    $("#uploadAvatars_form_div").show();  
    $("#add_upload").addClass("in");  
    $("#main_div").hide();
    $("#addedit_form_div").hide();
    
}

function addActions()
{
    stopEvent(event);
    var error = passwordError = false;
    var emailerror = false;
    var emailpattern = /^(([a-zA-Z0-9_.-]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z0-9]{2,4}))+([,;:](([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z0-9]{2,4}))+)*$/;
    var passwordpattern = /([a-zA-Z0-9*#@!]{6,20})/;                    
    $(".has-error").removeClass("has-error");
    $("#addedit_form input,textarea,select").each(function(i,node){
        if($(node).attr("required"))
        {
            if(node.value == "")
            {
                error = true;
                $(node).parent().addClass("has-error");

            }
            else
            {
                switch(node.name)
                {
                    case "email":
                        if(node.value != "")
                        {
                            if(!emailpattern.test(node.value))
                            {
                                error = true;
                                emailerror = true;
                                $(node).parent().addClass("has-error");
                            }
                        }
                    break;
                    case "old_password":
                        if (node.value == $("#password").val())
                        {
                            error = true;
                            passwordError = true;
                            $("#old_password").parent().addClass("has-error");
                            $("#password").parent().addClass("has-error");
                            toastr.error('Password/Old password should not be same','Message',{
                               timeOut: 5000,
                               "closeButton": true,
                               "tapToDismiss": false
                           });
                        }
                    break;
                    case "password":
                    case "password_confirmation":
                        if (node.value != $("#password_confirmation").val())
                        {
                           $(node).parent().addClass("has-error");
                           $("#password_confirmation").parent().addClass("has-error");
                           passwordError = true;
                           error = true;
                           toastr.error('Password/confirm password should be same','Message',{
                               timeOut: 5000,
                               "closeButton": true,
                               "tapToDismiss": false
                           });
                        }
                        else if(!passwordpattern.test(node.value))
                        {
                           $(node).parent().addClass("has-error");
                           $("#password_confirmation").parent().addClass("has-error");
                           passwordError = true;
                           error = true;
                           toastr.error('Password should contain atleast 6 characters.Allowed Special Characters are *#@! only!','Message',{
                               timeOut: 5000,
                               "closeButton": true,
                               "tapToDismiss": false
                           });
                        }
                        break;
                        
                }   
            }

        }
    });
    
    if(emailerror){
        toastr.error('Invalid Email','Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
    }
    if(!error)
    {
        showOverlay(1);
        var postData = $($("#addedit_form")).serializeArray();
        var formURL  = $($("#addedit_form")).attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            data : postData,
            success:function(data, textStatus, jqXHR) 
            {
                if(textStatus == "success")
                {
                    if(data.result)
                    {
                        $("#addedit_form_div").hide();
                        if(data.view)	
                        {	
                           $("#profileView").html(data.view);	
                           //$("#addedit_form_submit").click(addActions);	
                           //$("#addedit_form").submit(addActions); 	
                        }	
                         toastr.success(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                        if(data.action == "change_password"){
                            location.reload();
                        }	
                        if(typeof data.reload != "undefined" && data.reload){	
                            location.reload();	
                        }
                    }else{
                        hideOverlay(1);
                        toastr.error(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    }
                    
                }
                hideOverlay(1);
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                if(jqXHR.responseJSON.errors){	
                    $.each( jqXHR.responseJSON.errors, function( key, value) {	
                        toastr.error(value,'Error',{	
                            timeOut: 5000,	
                            "closeButton": true,	
                            "tapToDismiss": false	
                        });	
                    });	
                    	
                }else if(jqXHR.responseJSON.exception){	
                    toastr.error(jqXHR.responseJSON.message,'Error',{	
                        timeOut: 5000,	
                        "closeButton": true,	
                        "tapToDismiss": false	
                    });	
                }	
                hideOverlay(1);
            }
       });
    }else{
        toastr.error('Please fill Required Data','Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
        $('html, body').animate({
            scrollTop: $(".page-titles").offset().top
        }, 1000);
    }
}

function editActions(event)
{
    stopEvent(event);
    showOverlay(1);
    
    var formURL  = $($(event.target)).attr("href");
    $.ajax(
    {
        url : formURL,
        type: "GET",
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
                $("#addedit_form_div").show();
                $("#addedit_form_div").html(data.data);
                 $('html, body').animate({
                        scrollTop: $(".page-titles").offset().top
                    }, 1000);
                $('.panel-collapse').collapse('show');
                $("#addedit_form_submit").click(addActions);
                $("#addedit_form").submit(addActions);
                setTimeout(function(){	
                    togglePlusMinus();	
                },200);
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            if(jqXHR.responseJSON.errors){	
                $.each( jqXHR.responseJSON.errors, function( key, value) {	
                    toastr.error(value,'Error',{	
                        timeOut: 5000,	
                        "closeButton": true,	
                        "tapToDismiss": false	
                    });	
                });	
                	
            }else if(jqXHR.responseJSON.exception){	
                toastr.error(jqXHR.responseJSON.message,'Error',{	
                    timeOut: 5000,	
                    "closeButton": true,	
                    "tapToDismiss": false	
                });	
            }	
            hideOverlay(1);
        }
   });
}