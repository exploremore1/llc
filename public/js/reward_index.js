//Js is a shorthand notation of javascript
$().ready(function(){
    
    //Toggle PlusMinus
    togglePlusMinus();

    //bind datatable
    toggleDataTables("#list table");
     
    $("#addedit_form_submit").click(addActions);
    $("#addedit_form").submit(addActions);
   
    $("#filter_form_submit").click(submitForm);
    $("#filter_form").submit(submitForm);
    $("#filter_form_reset").click(function(event){
        $("#list").fadeOut("slow",function(){
        $("#list").html();

        $("#filter_title").val("");
        
        $('html, body').animate({
            scrollTop: $(".row").offset().top
        }, 1000);

        loadList();
    });
    });
    
    $(".upload_image").hide();  
    eventActions();

});

function submitForm(event)
{
    stopEvent(event);
    loadList();
}

function eventActions()
{
    $('#listTable').on('click', '.delete', function() {
        deleteActions(event);
    });
    $('#listTable').on('click', '.edit', function() {
        editActions(event);
    });
    $('#listTable').on('click', '.view', function() {
        viewActions(event);
    });

    //Initialize select2
    $('.select2').select2();
    $('.panel-collapse').collapse('hide');

    $('#from_business_owner').on('change', function() {
        getAssignedCustomers();
    });
    
}

function viewActions(event)
{
    stopEvent(event);
    showOverlay(1);
    
    var formURL  = $($(event.target)).attr("href");
    $.ajax(
    {
        url : formURL,
        type: "GET",
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
                $("#view_form_div").html(data.data);
                $('html, body').animate({
                    scrollTop: $(".page-titles").offset().top
                }, 1000);
                togglePlusMinus();
                $('.panel-collapse').collapse('hide');
                $('.viewDetails-collapse').collapse('show');
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            // if(jqXHR.responseJSON.errors.email)
            // {
            //     toastr.error(jqXHR.responseJSON.errors.email,'Message',{
            //         timeOut: 0,
            //         "closeButton": true,
            //         "tapToDismiss": false
            //     });
            // }
            hideOverlay(1);
        }
   });
}

function loadList(event)
{
    showOverlay(1);
    
    var postData = $($("#filter_form")).serializeArray();
    var formURL = $($("#filter_form")).attr("action");
    $.ajax(
    {
        url : formURL,
        type: "POST",
        data : postData,
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success" && data['result'])
            {
                $($("#addedit_form input,#addedit_form select,#addedit_form textarea")).each(function(i,node){  $(node).val(""); });
                $('.panel-collapse').collapse('hide');
                $("#list").html(data.data);
                $("#list").fadeIn("slow",function(){
                    $('html, body').animate({
                        scrollTop: $("#list").offset().top
                    }, 1000);
                    $('[data-toggle="tooltip"]').tooltip();
                    eventActions();
                });
                
                toggleDataTables("#list table");
                
            }else{
                toastr.error(data.data,'Message',{
                    timeOut: 5000,
                    "closeButton": true,
                    "tapToDismiss": false
                });
                $('html, body').animate({
                    scrollTop: $("#list").offset().top
                }, 1000);
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
        }
   });
}

function addActions()
{
    stopEvent(event);
    var error = productLinkError = false;
    $(".has-error").removeClass("has-error");
    $("#addedit_form input, #addedit_form textarea, #addedit_form select").each(function(i,node){
        if($(node).attr("required")){
            if(node.value == ""){
                error = true;
                $(node).parent().addClass("has-error");
            }
            else
            {
                switch(node.name)
                {
                    case "product_link":
                        if(node.value != "")
                        {
                            if(!isValidURL(node.value))
                            {
                                error = true;
                                productLinkError = true;
                                $(node).parent().addClass("has-error");
                            }
                        }
                    break;
                }
            } 
        }
    });
    
    if(productLinkError){
        toastr.error('Invalid Product Link','Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
    }
    
    if(!error)
    {
        showOverlay(1);

        //var postData = $($("#addedit_form")).serializeArray();
        var formURL  = $($("#addedit_form")).attr("action");

        // Get form
        var form = $('#addedit_form')[0];

        // Create an FormData object 
        var formData = new FormData(form);

        $.ajax(
        {
            url : formURL,
            type: "POST",
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            data : formData,
            processData: false,
            contentType: false,
            cache: false,
            success:function(data, textStatus, jqXHR) 
            {
                if(textStatus == "success")
                {
                    if(data.result)
                    {
                        if(data.view)
                        {
                           $("#addedit_form_div").html(data.view);
                           $("#addedit_form_submit").click(addActions);
                           $("#addedit_form").submit(addActions); 
                        }
                        $("#titles").html("ADD");
                        $("#addedit_form_submit").html("CREATE");
                        toastr.success(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                        loadList();
                    }else{
                        hideOverlay(1);
                        toastr.error(data.data,'Message',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    }
                    
                }
                hideOverlay(1);
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
                if(jqXHR.responseJSON.errors){
                    $.each( jqXHR.responseJSON.errors, function( key, value) {
                        toastr.error(value,'Error',{
                            timeOut: 5000,
                            "closeButton": true,
                            "tapToDismiss": false
                        });
                    });
                    
                }else if(jqXHR.responseJSON.exception){
                    toastr.error(jqXHR.responseJSON.message,'Error',{
                        timeOut: 5000,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                }
                hideOverlay(1);
            }
       });
    }else{
        toastr.error('Please fill Required Data','Message',{
            timeOut: 5000,
            "closeButton": true,
            "tapToDismiss": false
        });
        $('html, body').animate({
            scrollTop: $(".page-titles").offset().top
        }, 1000);
    }
}

function editActions(event)
{
    stopEvent(event);
    showOverlay(1);
    
    var formURL  = $($(event.target)).attr("href");
    $.ajax(
    {
        url : formURL,
        type: "GET",
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
                $("#addedit_form_div").html(data.data);
                //Initialize select2
                $('.select2').select2();
                
                 $('html, body').animate({
                        scrollTop: $(".page-titles").offset().top
                    }, 1000);

                togglePlusMinus();
                $('.panel-collapse').collapse('show');
                $("#addedit_form_submit").click(addActions);
                $("#addedit_form").submit(addActions);
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
            if(jqXHR.responseJSON.errors){
                $.each( jqXHR.responseJSON.errors, function( key, value) {
                    toastr.error(value,'Error',{
                        timeOut: 5000,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                });
                
            }else if(jqXHR.responseJSON.exception){
                toastr.error(jqXHR.responseJSON.message,'Error',{
                    timeOut: 5000,
                    "closeButton": true,
                    "tapToDismiss": false
                });
            }
            hideOverlay(1);
        }
   });
}

function deleteActions()
{
    if(!confirm("Are you sure to delete this?")){
      event.preventDefault();
        return true;
    }
    
    stopEvent(event);
    showOverlay(1);
    
    var formURL  = $($(event.target)).attr("href");
    $.ajax(
    {
        url : formURL,
        type: "GET",
        success:function(data, textStatus, jqXHR) 
        {
            if(textStatus == "success")
            {
                toastr.success('Record has been deleted successfully','Message',{
                    timeOut: 5000,
                    "closeButton": true,
                    "tapToDismiss": false
                });
                loadList();
            }
            hideOverlay(1);
        },
        error: function(jqXHR, textStatus, errorThrown) 
        {
        }
   });
}

function playSong(id){
    $("#play_song_"+id).slideToggle('1000');
}


function getAssignedCustomers(){
    var from_business_owner  = $("#from_business_owner").val();
    var arrVars = from_business_owner.split("/");
    var business_owner_id = arrVars[0];
    //var from_email = arrVars[1];

     var formURL  = $('#from_business_owner').attr('data-attribute')+'/'+business_owner_id+'/get_assigned_customers';
    $.ajax(
    {
        url : formURL,
        type: "GET",
          headers:{
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
          success:function(data, textStatus, jqXHR)  {
            if(textStatus == "success") {
                var customer_options = '<option value="">Select Customer</option>';
                if(data.length > 0) { 
                    $.each(data, function(index, value){
                        if(value.email != null || value.email != undefined ){
                            customer_options += '<option value="'+value.id+'/' + value.email +'" >' + value.fname +' '+value.lname+' ('+value.email+')</option>';
                        }
                    });
                }else{
                    hideOverlay(1);
                    toastr.error('No customer is assigned to this business owner.','Message',{
                        timeOut: 5000,
                        "closeButton": true,
                        "tapToDismiss": false
                    });
                }
                $('#to_customer').html(customer_options);
            }
            hideOverlay(1);
          },
          error: function(jqXHR, textStatus, errorThrown) 
          {
          }
     });
  
}


function isValidURL(string) {
    var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    return (res !== null)
}

  