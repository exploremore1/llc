<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BusinessOwnerController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\SongController;
use App\Http\Controllers\RewardController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::middleware(['auth:sanctum', 'verified'])->group(function () {
//     Route::get('/users/profile',  [UserController::class, 'index']);
// });

Route::get('/play/song/{id?}/{record_id?}', [SongController::class, 'playSong']);

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('/', [RewardController::class, 'index'])->name('rewards.list');

    Route::get('/users', [UserController::class, 'index'])->name('list');
    Route::post('find/users', [UserController::class, 'find']);
    Route::get('/users/profile', [UserController::class, 'profile'])->name('profile');
    Route::any('/edit/user/{id?}/{type?}', [UserController::class, 'edit'])->name('edit');
    Route::post('/uploadAvatars/user/{id?}', [UserController::class, 'uploadAvatars'])->name('uploadAvatars');
    Route::get('/delete/user/{id}', [UserController::class, 'delete'])->name('delete');
    Route::get('viewDetails/user/{id}', [UserController::class, 'view'])->name('view');

    Route::get('/business-owners', [BusinessOwnerController::class, 'index'])->name('business_owners.list');
    Route::post('find/business-owners', [BusinessOwnerController::class, 'find']);
    Route::any('/edit/business-owner/{id?}/{type?}', [BusinessOwnerController::class, 'edit'])->name('business_owners.edit');
    Route::get('/delete/business-owner/{id}', [BusinessOwnerController::class, 'delete'])->name('business_owners.delete');
    Route::get('viewDetails/business-owner/{id}', [BusinessOwnerController::class, 'view'])->name('business_owners.view');

    Route::get('/customers', [CustomerController::class, 'index'])->name('customers.list');
    Route::post('find/customers', [CustomerController::class, 'find']);
    Route::any('/edit/customer/{id?}/{type?}', [CustomerController::class, 'edit'])->name('customers.edit');
    Route::get('/delete/customer/{id}', [CustomerController::class, 'delete'])->name('customers.delete');
    Route::any('sendEmail/customer/{id}', [CustomerController::class, 'sendEmail'])->name('customer_send_email');
    Route::any('sendBulkEmail/customers/{type?}', [CustomerController::class, 'sendBulkEmail'])->name('customers_send_bulk_email');
    Route::any('sendSampleEmail/{type?}', [CustomerController::class, 'sendSampleEmail'])->name('customers_send_sample_email');
    Route::any('bulkImport/customers', [CustomerController::class, 'bulkImport'])->name('customers_bulk_import');
    Route::any('downloadSampleFileBulkImport/customers', [CustomerController::class, 'downloadSampleFileBulkImport'])->name('downloadSampleFileBulkImport');
    Route::get('viewDetails/customer/{id}', [CustomerController::class, 'view'])->name('customers.view');

    Route::get('/songs', [SongController::class, 'index'])->name('songs.list');
    Route::any('/edit/song/{id?}/{type?}', [SongController::class, 'edit'])->name('songs.edit');
    Route::get('/delete/song/{id}', [SongController::class, 'delete'])->name('songs.delete');
    Route::post('find/songs', [SongController::class, 'find']);
    Route::get('view/songs/{id}', [SongController::class, 'viewPdf'])->name('songs.pdf_view');
    Route::get('viewDetails/song/{id}', [SongController::class, 'view'])->name('songs.view');
    
    Route::get('/rewards', [RewardController::class, 'index'])->name('rewards.list');
    Route::any('/edit/reward/{id?}/{type?}', [RewardController::class, 'edit'])->name('rewards.edit');
    Route::get('/delete/reward/{id}', [RewardController::class, 'delete'])->name('rewards.delete');
    Route::post('find/rewards', [RewardController::class, 'find']);
    Route::get('view/reward/{id}', [RewardController::class, 'view'])->name('rewards.view');
});
